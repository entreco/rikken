# Rikken

Project to make the Dutch card game "Rikken" available on all platforms.

[![Coverage Status](https://coveralls.io/repos/gitlab/entreco/rikken/badge.svg?branch=master)](https://coveralls.io/gitlab/entreco/rikken?branch=master)

## Idea:
1. Create Kotlin multiplatform domain model
1. Create libraries for Jvm, Native and Js
1. Create frontends for Android,iOS & Web

## Architecture

[See mermaid](https://mermaidjs.github.io/#/)

```mermaid
graph TD
    Server[fa:fa-server Rikken Server] -->|uses| Core[fa:fa-client Rikken Core]
    Client[fa:fa-file-code-o Rikken Client] -->|uses| Core
    Android[fa:fa-mobile Android] -->|uses| Client
    Web[fa:fa-user Web] -->|uses| Client
    Desktop[fa:fa-desktop Desktop] -->|uses| Client
  
```

## Core

[See Core](https://gitlab.com/entreco/rikken/-/tree/develop/core)

KMM Library, containing all business logic to play a game of Rikken.

```kotlin
val rikGame = RikGame(rikView, rikController).apply {
  registerHandlers(::onEvent, ::onQuit)
}

private fun onQuit(message: String) {
  println(message)
  exitProcess(1)
}

private suspend fun onEvent(rikGame: RikGame, player: Player?) {
  val line = readLine()
  rikGame.onCommand(player?.id ?: "Player ${players.size + 1}", line)
}
```

## Server

[See Server](https://gitlab.com/entreco/rikken/-/tree/develop/server)

Ktor Server Application. Handles incoming connections and starts a game
once all players have joined. Each client can either create a new Room, 
and wait for others to join. OR they can join an existing room.

Currently, WebSockets are used to communicate between connected clients.
Below is a snippet of the main logic to handle messages.

```kotlin
try {
  rikGame.registerHandlers({ _, _ -> }, { close(CloseReason(CloseReason.Codes.GOING_AWAY, it))})
  rikView += session
  rikGame.onJoin(session.id, session.identifier, session.username)

  incoming.consumeEach { frame ->
    if (frame is Frame.Text) {
      rikGame.onCommand(session.id, frame.readText())
    }
  }
} finally {
  rikView -= session
}
```

## Client

[See Client](https://gitlab.com/entreco/rikken/-/tree/develop/client)

KMM Library, which can be used on all platforms to communicate with the Server.
Feel free to write a client for your favorite platform:

### Get Started

```gradle
repositories {
  mavenCentral()
}

dependencies {
  implementation "nl.entreco.rikken:rikken-client:0.0.1"
}
```

Create a new client, and start playing

```gradle
val client = DefaultRikClient(ServerConfig())
    
println("Please enter your name:")
client.play(room = "123", name = readLine().orEmpty())
```

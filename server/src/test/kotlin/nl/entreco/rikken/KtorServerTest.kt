package nl.entreco.rikken

import RikGame
import RikHandler
import RoomID
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.test.assertTrue

class KtorServerTest {

    @Test
    fun shouldBeAbleToJoinServer() = runTest {
        val roomId = RoomID.random()
        val subject = givenKtorServer()
        subject.join(testHandler(), roomId)
        assertTrue(subject.state is RikGame.State.Waiting)
        subject.join(testHandler(), roomId)
        assertTrue(subject.state is RikGame.State.Waiting)
        subject.join(testHandler(), roomId)
        assertTrue(subject.state is RikGame.State.Waiting)
        subject.join(testHandler(), roomId)
        assertTrue(subject.state is RikGame.State.Bidding)
    }

    private fun givenKtorServer() = KtorServer()
    private fun testHandler(): RikHandler = object : RikHandler {
        override val username = "testUser"

        override suspend fun chat(chat: RikGame.Event.Chat) {
        }

        override suspend fun whisper(state: RikGame.State) {
        }
    }
}
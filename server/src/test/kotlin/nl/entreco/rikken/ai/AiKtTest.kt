package nl.entreco.rikken.ai

import core.rikken.Card
import core.rikken.Rank
import core.rikken.RikSettings
import core.rikken.Setting
import core.rikken.Suit
import org.junit.Assert.assertEquals
import org.junit.Test

class AiKtTest {

    val H2 = Card(Rank.TWO, Suit.HEARTS)
    val S2 = Card(Rank.TWO, Suit.SPADES)
    val R2 = Card(Rank.TWO, Suit.DIAMONDS)
    val K2 = Card(Rank.TWO, Suit.CLUBS)

    val H3 = Card(Rank.THREE, Suit.HEARTS)
    val S3 = Card(Rank.THREE, Suit.SPADES)
    val R3 = Card(Rank.THREE, Suit.DIAMONDS)
    val K3 = Card(Rank.THREE, Suit.CLUBS)

    val H4 = Card(Rank.FOUR, Suit.HEARTS)
    val S4 = Card(Rank.FOUR, Suit.SPADES)
    val R4 = Card(Rank.FOUR, Suit.DIAMONDS)
    val K4 = Card(Rank.FOUR, Suit.CLUBS)

    val H5 = Card(Rank.FIVE, Suit.HEARTS)
    val S5 = Card(Rank.FIVE, Suit.SPADES)
    val R5 = Card(Rank.FIVE, Suit.DIAMONDS)
    val K5 = Card(Rank.FIVE, Suit.CLUBS)

    val H6 = Card(Rank.SIX, Suit.HEARTS)
    val S6 = Card(Rank.SIX, Suit.SPADES)
    val R6 = Card(Rank.SIX, Suit.DIAMONDS)
    val K6 = Card(Rank.SIX, Suit.CLUBS)

    val H7 = Card(Rank.SEVEN, Suit.HEARTS)
    val S7 = Card(Rank.SEVEN, Suit.SPADES)
    val R7 = Card(Rank.SEVEN, Suit.DIAMONDS)
    val K7 = Card(Rank.SEVEN, Suit.CLUBS)

    val H8 = Card(Rank.EIGHT, Suit.HEARTS)
    val S8 = Card(Rank.EIGHT, Suit.SPADES)
    val R8 = Card(Rank.EIGHT, Suit.DIAMONDS)
    val K8 = Card(Rank.EIGHT, Suit.CLUBS)

    val H9 = Card(Rank.NINE, Suit.HEARTS)
    val S9 = Card(Rank.NINE, Suit.SPADES)
    val R9 = Card(Rank.NINE, Suit.DIAMONDS)
    val K9 = Card(Rank.NINE, Suit.CLUBS)

    val H10 = Card(Rank.TEN, Suit.HEARTS)
    val S10 = Card(Rank.TEN, Suit.SPADES)
    val R10 = Card(Rank.TEN, Suit.DIAMONDS)
    val K10 = Card(Rank.TEN, Suit.CLUBS)

    val HJ = Card(Rank.JACK, Suit.HEARTS)
    val SJ = Card(Rank.JACK, Suit.SPADES)
    val RJ = Card(Rank.JACK, Suit.DIAMONDS)
    val KJ = Card(Rank.JACK, Suit.CLUBS)

    val HQ = Card(Rank.QUEEN, Suit.HEARTS)
    val SQ = Card(Rank.QUEEN, Suit.SPADES)
    val RQ = Card(Rank.QUEEN, Suit.DIAMONDS)
    val KQ = Card(Rank.QUEEN, Suit.CLUBS)

    val HK = Card(Rank.KING, Suit.HEARTS)
    val SK = Card(Rank.KING, Suit.SPADES)
    val RK = Card(Rank.KING, Suit.DIAMONDS)
    val KK = Card(Rank.KING, Suit.CLUBS)

    val HA = Card(Rank.ACE, Suit.HEARTS)
    val SA = Card(Rank.ACE, Suit.SPADES)
    val RA = Card(Rank.ACE, Suit.DIAMONDS)
    val KA = Card(Rank.ACE, Suit.CLUBS)

    @Test
    fun `should suggest Troef for single Card`(){
        val givenCards = listOf(HA)
        val suit = givenCards.suggestTroef()
        assertEquals(Suit.HEARTS, suit)
    }

    @Test
    fun `should suggest Troef for multiple Card`(){
        val givenCards = listOf(HA, HK, HQ, S2, S3, S4, S5, S6, S7, S8, S9)
        val suit = givenCards.suggestTroef()
        assertEquals(Suit.HEARTS, suit)
    }

    @Test
    fun `should suggest Troef for multiple Card - swapped`(){
        val givenCards = listOf(KA, KK, KQ, S2, S3, S4, S5, S6, S7, S8, S9)
        val suit = givenCards.suggestTroef()
        assertEquals(Suit.CLUBS, suit)
    }

    @Test
    fun `should suggest Troef for multiple Card - threshold`(){
        val givenCards = listOf(KA, KK, KQ, S2, S3, S4, S5, S6, S7, S8, S9, S10)
        val suit = givenCards.suggestTroef()
        assertEquals(Suit.SPADES, suit)
    }

    @Test
    fun `should suggest Mate for multiple Card - threshold`(){
        val givenCards = listOf(KA, KK, KQ, S2, S3, S4, S5, S6, S7, S8, S9, S10)
        val suit = givenCards.suggestMate(givenCards.suggestTroef(), emptyMap())
        assertEquals(Suit.CLUBS, suit)
    }
}
package nl.entreco.rikken

import PlayerId
import RikClient
import RikGame
import RikHandler
import RoomID
import RoomID.Companion.asRoomID
import app.cash.turbine.test
import core.rikken.RikType
import core.rikken.Suit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.test.runTest
import nl.entreco.rikken.ai.RandomAi
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class ClientServerTest {

    @Test
    fun shouldJoinExistingRoom() = runTest {
        val server = KtorServer(RandomAi())
        val client1 = Client("Remco")
        client1.state.test {
            var state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Welcome)

            server.join(client1, client1.room)
            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Waiting)
            assertTrue(state1.playersRemaining == 3)
            assertEquals(0, state1.index)

            val client2 = Client("Desktop", state1.code.code)
            client2.state.test {
                var state2 = awaitItem()
                assertTrue(state2 is RikGame.State.Welcome)

                server.join(client2, client2.room)
                state2 = awaitItem()
                assertTrue(state2 is RikGame.State.Waiting)
                assertTrue(state2.playersRemaining == 2)
                assertEquals(1, state2.index)
            }

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Waiting)
            assertTrue(state1.playersRemaining == 2)
            assertEquals(0, state1.index)

            val client3 = Client("WebAssem", state1.code.code)
            client3.state.test {
                var state3 = awaitItem()
                assertTrue(state3 is RikGame.State.Welcome)

                server.join(client3, client3.room)
                state3 = awaitItem()
                assertTrue(state3 is RikGame.State.Waiting)
                assertTrue(state3.playersRemaining == 1)
                assertEquals(2, state3.index)
            }

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Waiting)
            assertTrue(state1.playersRemaining == 1)
            assertEquals(0, state1.index)

            val client4 = Client("iOS", state1.code.code)
            client4.state.test {
                var state4 = awaitItem()
                assertTrue(state4 is RikGame.State.Welcome)

                server.join(client4, client4.room)
                state4 = awaitItem()
                assertTrue(state4 is RikGame.State.Waiting)
                assertTrue(state4.playersRemaining == 0)
                assertEquals(3, state4.index)

                state4 = awaitItem()
                assertTrue(state4 is RikGame.State.Bidding)
            }

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Waiting)
            assertTrue(state1.playersRemaining == 0)
            assertEquals(0, state1.index)


            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
        }
    }

    @Test
    fun shouldStartGameWithBots() = runTest {
        val server = KtorServer(RandomAi())
        val client1 = Client("Remco")
        client1.state.test {
            var state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Welcome)
            server.join(client1, client1.room)

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Waiting)
            server += RikGame.Event.Begin(state1.code)
            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertTrue(state1.players.size == 4)
            assertFalse(state1.players[0].bot)
            assertTrue(state1.players[1].bot)
            assertTrue(state1.players[2].bot)
            assertTrue(state1.players[3].bot)
        }
    }

    @Test
    fun shouldPlaceFirstBid() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertTrue(state1.bids.all { it.value is RikType.None })
        }
        client2.state.test {
            val state2 = awaitItem()
            assertTrue(state2 is RikGame.State.Bidding)
            assertTrue(state2.bids.all { it.value is RikType.None })
        }
        client3.state.test {
            val state3 = awaitItem()
            assertTrue(state3 is RikGame.State.Bidding)
            assertTrue(state3.bids.all { it.value is RikType.None })
        }
        client4.state.test {
            val state4 = awaitItem()
            assertTrue(state4 is RikGame.State.Bidding)
            assertTrue(state4.bids.all { it.value is RikType.None })
        }

        // Bid Rikken
        server += RikGame.Event.Bid(client1.playerId, RikType.Rik)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            val bids = state1.bids
            assertEquals(4, bids.size)
            assertEquals(RikType.Rik, bids[0])
            assertEquals(RikType.None, bids[1])
            assertEquals(RikType.None, bids[2])
            assertEquals(RikType.None, bids[3])
        }
    }

    @Test
    fun shouldPlaceBidsForAllPlayers() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertTrue(state1.bids.all { it.value is RikType.None })
        }
        client2.state.test {
            val state2 = awaitItem()
            assertTrue(state2 is RikGame.State.Bidding)
            assertTrue(state2.bids.all { it.value is RikType.None })
        }
        client3.state.test {
            val state3 = awaitItem()
            assertTrue(state3 is RikGame.State.Bidding)
            assertTrue(state3.bids.all { it.value is RikType.None })
        }
        client4.state.test {
            val state4 = awaitItem()
            assertTrue(state4 is RikGame.State.Bidding)
            assertTrue(state4.bids.all { it.value is RikType.None })
        }

        // Bid Rikken
        server += RikGame.Event.Bid(client1.playerId, RikType.Rik)
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client4.playerId, RikType.Pas)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Selecting)
        }
    }

    @Test
    fun shouldPlaceBidsForBotsPlayers() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        server += RikGame.Event.Begin(room)

        client1.state.test {
            var state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)

            // Bid Pas
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertNotEquals(client1.playerId.index, state1.biddingPlayer)
            assertTrue(cancelAndConsumeRemainingEvents().size > 2)
        }
    }

    @Test
    fun shouldRejectBidsIfNotYourTurn() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Kwikske", room.code)
        server.join(client2, room)
        server += RikGame.Event.Begin(room)

        // Should be rejected
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)

        client1.state.test {
            var state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertTrue(state1.bids.all { it.value is RikType.None })
            assertEquals(client2.playerId.index, state1.error?.to)

            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)

            state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Bidding)
            assertTrue(state1.bids.count { it.value is RikType.Pas } == 1)
            assertEquals(null, state1.error)
        }
    }

    @Test
    fun shouldAcceptOneConfirmEvent() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Kwikske", room.code)
        server.join(client2, room)
        server += RikGame.Event.Begin(room)
        client1.state.test {
            assertTrue(awaitItem() is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            assertTrue(awaitItem() is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
            assertTrue(awaitItem() is RikGame.State.Bidding)
            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    fun shouldPlaySingleCard() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        // Bids
        server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client2.playerId, RikType.Rik)
        server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client4.playerId, RikType.Pas)

        // Confirm
        server += RikGame.Event.Confirm.Rik(client2.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)

        // Playing card 0
        server += RikGame.Event.Play(client1.playerId, 0)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Playing)
            assertEquals(1, state1.played.size)
            assertEquals(12, state1.hands[0].size)
            assertEquals(13, state1.hands[1].size)
            assertEquals(13, state1.hands[2].size)
            assertEquals(13, state1.hands[3].size)
        }

        client2.state.test {
            val state2 = awaitItem()
            assertTrue(state2 is RikGame.State.Playing)
            assertEquals(1, state2.played.size)
            assertEquals(12, state2.hands[0].size)
            assertEquals(13, state2.hands[1].size)
            assertEquals(13, state2.hands[2].size)
            assertEquals(13, state2.hands[3].size)
        }
    }

    @Test
    fun shouldPlayMultipleCards() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        // Bids
        server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client3.playerId, RikType.Rik)
        server += RikGame.Event.Bid(client4.playerId, RikType.Pas)

        // Confirm
        server += RikGame.Event.Confirm.Rik(client3.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)

        // Playing card 0
        server += RikGame.Event.Play(client1.playerId, 0)
        server += RikGame.Event.Play(client2.playerId, 0)
        server += RikGame.Event.Play(client3.playerId, 0)

        client1.state.test {
            val state = awaitItem()
            assertTrue(state is RikGame.State.Playing)
            assertEquals(3, state.played.size)
            assertEquals(12, state.hands[0].size)
            assertEquals(12, state.hands[1].size)
            assertEquals(12, state.hands[2].size)
            assertEquals(13, state.hands[3].size)
        }
    }

    @Test
    fun shouldPlayMultipleRounds() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        // Bids
        server += RikGame.Event.Bid(client1.playerId, RikType.Rik)
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client4.playerId, RikType.Pas)

        // Confirm
        server += RikGame.Event.Confirm.Rik(client1.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)

        // Playing card 0
        server += RikGame.Event.Play(client1.playerId, 0)
        server += RikGame.Event.Play(client2.playerId, 0)
        server += RikGame.Event.Play(client3.playerId, 0)
        server += RikGame.Event.Play(client4.playerId, 0)

        client1.state.test {
            val state = awaitItem()
            assertTrue(state is RikGame.State.Playing)
            assertEquals(0, state.played.size.mod(4))
            assertEquals(12, state.hands[0].size)
            assertEquals(12, state.hands[1].size)
            assertEquals(12, state.hands[2].size)
            assertEquals(12, state.hands[3].size)
        }
    }

    @Test
    @Ignore("TODO: Fix")
    fun shouldPlayCompleteLeg() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        server += RikGame.Event.Bid(client1.playerId, RikType.Rik)
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client4.playerId, RikType.Pas)

        server += RikGame.Event.Confirm.Rik(client1.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)

        // Playing card 0
        repeat(12) {
            server += RikGame.Event.Play(client1.playerId, 0)
            server += RikGame.Event.Play(client2.playerId, 0)
            server += RikGame.Event.Play(client3.playerId, 0)
            server += RikGame.Event.Play(client4.playerId, 0)
        }


        server += RikGame.Event.Play(client1.playerId, 0)
        server += RikGame.Event.Play(client2.playerId, 0)
        server += RikGame.Event.Play(client3.playerId, 0)
        server += RikGame.Event.Play(client4.playerId, 0)

        client1.state.test {
            val result = awaitItem()
            assertTrue(result is RikGame.State.Settling)
        }

        client2.state.test {
            val result = awaitItem()
            assertTrue(result is RikGame.State.Settling)
        }

        client3.state.test {
            val result = awaitItem()
            assertTrue(result is RikGame.State.Settling)
        }

        client4.state.test {
            val result = awaitItem()
            assertTrue(result is RikGame.State.Settling)
        }
    }

    @Test
    @Ignore("TODO: Fix")
    fun shouldPlayForBotsAutomatically() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        client1.state.test {
            var state = awaitItem()
            server += RikGame.Event.Begin(room)

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            assertEquals(0, (state as RikGame.State.Bidding).biddingPlayer)
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)

            while (state !is RikGame.State.Selecting) {
                state = awaitItem()
            }

            assertTrue("State: $state") { state is RikGame.State.Selecting }
            assertEquals(0, (state as RikGame.State.Selecting).startingPlayer)

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Playing }
            assertEquals(0, (state as RikGame.State.Playing).startingPlayer)
            server += RikGame.Event.Play(client1.playerId, 0)

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Playing }
            assertEquals(1, (state as RikGame.State.Playing).played.size)

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Playing }
            assertEquals(2, (state as RikGame.State.Playing).played.size)

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Playing }
            assertEquals(3, (state as RikGame.State.Playing).played.size)

            cancelAndIgnoreRemainingEvents()
        }
    }

    @Test
    @Ignore("TODO: Fix")
    fun shouldPlayMultipleLegs() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        client1.state.test {
            var state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client4.playerId, RikType.Rik)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            state = awaitItem()
            assertTrue(state is RikGame.State.Selecting)
            server += RikGame.Event.Confirm.Rik(client3.playerId, Suit.DIAMONDS, Suit.HEARTS, RikType.Rik)
            state = awaitItem()
            assertTrue(state is RikGame.State.Playing)

            // Playing card 0
            while (state is RikGame.State.Playing && state.played.size < 13 * 4) {
                server += RikGame.Event.Play(client1.playerId, 0)
                state = awaitItem()
                assertTrue(state is RikGame.State.Playing)
                server += RikGame.Event.Play(client2.playerId, 0)
                state = awaitItem()
                assertTrue(state is RikGame.State.Playing)
                server += RikGame.Event.Play(client3.playerId, 0)
                state = awaitItem()
                assertTrue(state is RikGame.State.Playing)
                server += RikGame.Event.Play(client4.playerId, 0)
                state = awaitItem()
                assertTrue(state is RikGame.State.Playing)
            }

            state = awaitItem()
            assertTrue(state is RikGame.State.Settling)
            server += RikGame.Event.Begin(room)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client2.playerId, RikType.Rik)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client4.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue(state is RikGame.State.Bidding)
            state = awaitItem()
            assertTrue(state is RikGame.State.Selecting)
            server += RikGame.Event.Confirm.Rik(client2.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)
            state = awaitItem()
            assertTrue(state is RikGame.State.Playing)
            assertTrue(state.players.size == 4)
            assertTrue(state.startingPlayer == 1)
            assertFalse(state.players[0].bot)
            assertFalse(state.players[1].bot)
            assertFalse(state.players[2].bot)
            assertFalse(state.players[3].bot)
        }
    }

    @Test
    @Ignore("TODO: Fix")
    fun shouldPlayMultipleLegsWithBots() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)

        server += RikGame.Event.Begin(room)

        client1.state.test {

            var state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            while (state !is RikGame.State.Playing) {
                state = awaitItem()
            }

            while (state is RikGame.State.Playing && state.played.size < 13 * 4) {
                server += RikGame.Event.Play(client1.playerId, 0)
                state = awaitItem()
                server += RikGame.Event.Play(client2.playerId, 0)
                state = awaitItem()
                state = awaitItem()
                state = awaitItem()
            }

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Settling }

            server += RikGame.Event.Begin(room)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            server += RikGame.Event.Bid(client2.playerId, RikType.Solo13Beter)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Selecting }
            server += RikGame.Event.Confirm.Rik(client2.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)
            state = awaitItem()
            assertTrue(state is RikGame.State.Playing)
            assertEquals(1, state.startingPlayer)
            assertTrue(state.players.size == 4)
            assertFalse(state.players[0].bot)
            assertFalse(state.players[1].bot)
            assertTrue(state.players[2].bot)
            assertTrue(state.players[3].bot)
        }
    }

    @Test
    @Ignore("TODO: Fix")
    fun shouldPlayMultipleLegsWithBotsAndHumanPlayersPassing() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)

        server += RikGame.Event.Begin(room)
        client1.state.test {

            var state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }

            while (state !is RikGame.State.Playing) {
                state = awaitItem()
            }

            while (state is RikGame.State.Playing && state.played.size < 13 * 4) {
                server += RikGame.Event.Play(client1.playerId, 0)
                state = awaitItem()
                server += RikGame.Event.Play(client2.playerId, 0)
                state = awaitItem()
                state = awaitItem()
                state = awaitItem()
            }

            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Settling }

            server += RikGame.Event.Begin(room)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            state = awaitItem()
            assertTrue("State: $state") { state is RikGame.State.Bidding }
            server += RikGame.Event.Bid(client1.playerId, RikType.Pas)

            while (state !is RikGame.State.Selecting) {
                state = awaitItem()
            }

            assertTrue("State: $state") { state is RikGame.State.Selecting }
            state = awaitItem()
            assertTrue(state is RikGame.State.Playing)
            assertTrue(state.players.size == 4)
            assertEquals(1, state.startingPlayer)
            assertFalse(state.players[0].bot)
            assertFalse(state.players[1].bot)
            assertTrue(state.players[2].bot)
            assertTrue(state.players[3].bot)
        }
    }

    @Test
    fun shouldRejectCardsWhenItIsNotPlayersTurn() = runTest {
        val server = KtorServer(RandomAi())
        val room = RoomID.random()
        val client1 = Client("Remco", room.code)
        server.join(client1, room)
        val client2 = Client("Shotta", room.code)
        server.join(client2, room)
        val client3 = Client("Rudi", room.code)
        server.join(client3, room)
        val client4 = Client("vAsch", room.code)
        server.join(client4, room)

        server += RikGame.Event.Bid(client1.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client2.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client3.playerId, RikType.Pas)
        server += RikGame.Event.Bid(client4.playerId, RikType.Rik)
        server += RikGame.Event.Confirm.Rik(client4.playerId, Suit.HEARTS, Suit.SPADES, RikType.Rik)

        // Playing card 0
        server += RikGame.Event.Play(client2.playerId, 0)

        client1.state.test {
            val state1 = awaitItem()
            assertTrue(state1 is RikGame.State.Playing)
            assertEquals(0, state1.played.size)
            assertEquals(13, state1.hands[0].size)
            assertEquals(13, state1.hands[1].size)
            assertEquals(13, state1.hands[2].size)
            assertEquals(13, state1.hands[3].size)
        }

        client2.state.test {
            var state2 = awaitItem()
            assertTrue(state2 is RikGame.State.Playing)
            assertEquals(0, state2.played.size)
            assertEquals(13, state2.hands[0].size)
            assertEquals(13, state2.hands[1].size)
            assertEquals(13, state2.hands[2].size)
            assertEquals(13, state2.hands[3].size)
        }
    }


    data class Client(
        override val username: String,
        private val _room: String = ""
    ) : RikClient, RikHandler {
        private val _state = MutableStateFlow<RikGame.State>(RikGame.State.Welcome("none"))
        override val state = _state.asStateFlow()
        override val room: RoomID = _room.asRoomID()
        private var me: Int = -1
        override val playerId: PlayerId
            get() = PlayerId(me)

        override suspend fun chat(chat: RikGame.Event.Chat) {

        }

        override suspend fun whisper(state: RikGame.State) {
            if (state is RikGame.State.Waiting) {
                me = state.index
            }
            _state.value = state
        }

        override val events: Flow<List<RikGame.Event>>
            get() = TODO("Not yet implemented")

        override fun chat(msg: String) {}

        override suspend fun start() {}

        override fun begin() {}

        override fun bid(bid: RikType) {}

        override fun confirm(troef: Suit?, maat: Suit?, rikType: RikType) {}

        override fun play(cardIndex: Int) {}

        override fun stop() {}
    }
}
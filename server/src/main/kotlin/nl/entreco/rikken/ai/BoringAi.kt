package nl.entreco.rikken.ai

import PlayerId
import core.rikken.Actions
import core.rikken.Battle
import core.rikken.Card
import core.rikken.RikType
import core.rikken.Setting
import core.rikken.Suit

class BoringAi : Ai {

    override suspend fun generateBid(bids: Map<Int, RikType>, cards: List<Card>): RikType =
        RikType.Pas

    override suspend fun generateConfirmation(
        playerId: PlayerId,
        actions: Actions,
        cards: List<Card>
    ): RikGame.Event.Confirm {
        return when(actions){
            is Actions.ConfirmMieen -> RikGame.Event.Confirm.Mieeen(playerId)
            is Actions.ConfirmNoRik -> RikGame.Event.Confirm.NoRik(playerId, actions.rikType)
            is Actions.ConfirmRik.Normal -> RikGame.Event.Confirm.Rik(playerId, Suit.HEARTS, Suit.DIAMONDS, actions.rikType)
            is Actions.ConfirmRik.Solo -> RikGame.Event.Confirm.Solo(playerId, Suit.HEARTS, actions.rikType)
        }
    }

    override suspend fun generatePlay(
        playerIndex: Int,
        cards: List<Card>,
        played: List<Pair<Int, Card>>,
        battles: List<Battle>,
        allowed: List<Card>,
        settings: Setting
    ): Int {
        return cards.indexOf(allowed[0])
    }
}
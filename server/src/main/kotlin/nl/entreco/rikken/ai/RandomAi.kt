package nl.entreco.rikken.ai

import PlayerId
import RikGame
import core.rikken.Actions
import core.rikken.Battle
import core.rikken.Card
import core.rikken.RikType
import core.rikken.RikTypeRules
import core.rikken.Setting
import core.rikken.Suit
import kotlinx.coroutines.delay
import kotlin.random.Random

internal class RandomAi : Ai {
    override suspend fun generateBid(bids: Map<Int, RikType>, cards: List<Card>): RikType {
        // Simulate thinking...
        delay(Random.nextInt(10, 30).toLong())

        // Random select
        val options = RikTypeRules.allowed(bids)
        return options.random()
    }

    override suspend fun generateConfirmation(
        playerId: PlayerId,
        actions: Actions,
        cards: List<Card>
    ): RikGame.Event.Confirm {

        // Simulate thinking...
        delay(Random.nextInt(10, 30).toLong())

        return when (actions) {
            is Actions.ConfirmMieen -> RikGame.Event.Confirm.Mieeen(playerId)
            is Actions.ConfirmNoRik -> RikGame.Event.Confirm.NoRik(playerId, actions.rikType)
            is Actions.ConfirmRik.Normal -> RikGame.Event.Confirm.Rik(
                playerId,
                Suit.entries.toTypedArray().random(),
                Suit.entries.toTypedArray().random(),
                actions.rikType
            )

            is Actions.ConfirmRik.Solo -> RikGame.Event.Confirm.Solo(
                playerId,
                Suit.entries.toTypedArray().random(),
                actions.rikType
            )
        }
    }

    override suspend fun generatePlay(
        playerIndex: Int,
        cards: List<Card>,
        played: List<Pair<Int, Card>>,
        battles: List<Battle>,
        allowed: List<Card>,
        settings: Setting
    ): Int {
        // Simulate thinking...
        delay(Random.nextInt(10, 30).toLong())

        return cards.indexOf(allowed.random())
    }
}
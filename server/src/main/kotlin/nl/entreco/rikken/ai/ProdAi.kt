package nl.entreco.rikken.ai

import PlayerId
import RikGame
import core.rikken.Actions
import core.rikken.Battle
import core.rikken.Card
import core.rikken.Rank
import core.rikken.RikType
import core.rikken.RikTypeRules
import core.rikken.Setting
import core.rikken.Suit

class ProdAi : Ai {
    override suspend fun generateBid(bids: Map<Int, RikType>, cards: List<Card>): RikType {
        return RikTypeRules.allowed(bids).firstNotNullOfOrNull { bid ->
            when (bid) {
                RikType.Misere -> if (cards.count { it.rank == Rank.TWO } == 4) bid else null
                RikType.Rik -> canRik(cards, bid)
                RikType.RikBeter -> canRik(cards.filter { it.suit == Suit.HEARTS }, bid)
                RikType.Piek -> null // TODO: When do we want to Piek
                RikType.Misere -> null // TODO: When do we want to Misere
                else -> null
            }
        } ?: RikType.Pas
    }

    private fun canRik(
        cards: List<Card>,
        bid: RikType,
        a: Int = 6,
        b: Int = 38,
    ) = if (cards.groupBy { it.suit }.any { it.value.size >= a || it.value.sumOf { it.rank.ordinal } >= b }) bid else null

    override suspend fun generateConfirmation(
        playerId: PlayerId,
        actions: Actions,
        cards: List<Card>
    ): RikGame.Event.Confirm {
        return when (actions) {
            is Actions.ConfirmMieen -> RikGame.Event.Confirm.Mieeen(playerId)
            is Actions.ConfirmNoRik -> RikGame.Event.Confirm.NoRik(playerId, actions.rikType)
            is Actions.ConfirmRik.Normal -> {
                val troef = cards.suggestTroef()
                RikGame.Event.Confirm.Rik(
                    playerId,
                    troef,
                    cards.suggestMate(troef, actions.excludes),
                    actions.rikType
                )
            }

            is Actions.ConfirmRik.Solo -> RikGame.Event.Confirm.Solo(
                playerId,
                cards.suggestTroef(),
                actions.rikType
            )
        }
    }

    override suspend fun generatePlay(
        playerIndex: Int,
        cards: List<Card>,
        played: List<Pair<Int, Card>>,
        battles: List<Battle>,
        allowed: List<Card>,
        settings: Setting
    ): Int = try {

        // Shortcut if 1 option is allowed
        if (allowed.size == 1) cards.indexOf(allowed.first())
        else {
            println("================== What to do?")
            println("Me: $playerIndex")
            println("Settings: $settings")
            println("Last: ${battles.lastOrNull()}")
            println("Allowed: $allowed")
            if (settings.players().contains(playerIndex)) { // Playing
                when (settings) {
                    is Setting.Rik -> generateRik(cards, allowed, battles, played, settings)
                    is Setting.NoRik -> cards.indexOf(allowed.random())
                    else -> cards.indexOf(allowed.random())
                }
            } else { // Opposition
                when (settings) {
                    is Setting.Mieeen -> if (played.size < 4 * 5) cards.indexOf(allowed.maxBy { it.rank }) else if (played.map { it.second }.contains(Card(Rank.QUEEN, Suit.SPADES))) cards.indexOf(allowed.maxBy { it.rank }) else cards.indexOf(allowed.random())
                    is Setting.Rik -> if(cards.contains(settings.mate)) generateRik(cards, allowed, battles, played, settings) else generateOpposition(cards, allowed, battles, played, settings)
                    is Setting.Solo -> cards.indexOf(allowed.first())
                    is Setting.NoRik -> cards.indexOf(allowed.random())
                    else -> cards.indexOf(allowed.random())
                }
            }
        }
    } catch (oops: Exception) {
        cards.indexOf(allowed.random())
    }

    private fun generateRik(
        cards: List<Card>,
        allowed: List<Card>,
        battles: List<Battle>,
        played: List<Pair<Int, Card>>,
        settings: Setting.Rik
    ): Int {
        val isOpening = battles.lastOrNull()?.isEmpty() ?: true
        val isClosing = battles.lastOrNull()?.isLast() ?: false
        val troefPlayed = played.count { it.second.suit == settings.troef }
        val myTroef = cards.count { it.suit == settings.troef }
        val unknownMaat = settings.maat == null
        val maatTroef = played.filter { it.first == settings.maat }.count { it.second.suit == settings.troef }
        val troefLeft = 13 - troefPlayed - myTroef
        val (potenialWinner, card) = battles.last().highest()
        val suggest : Card? = when {
            // Zetten
            isOpening && unknownMaat -> allowed.firstOrNull { it.suit == settings.mate.suit } ?: allowed.random() // Maat vragen
            isOpening && troefLeft > 0 -> allowed.lastOrNull { it.suit == settings.troef } ?: allowed.first() // Troef trekken

            // Afmaken
            isClosing && potenialWinner == settings.maat -> allowed.first()
            isClosing && potenialWinner != settings.maat -> allowed.firstOrNull { it.suit == card.suit && it.rank > card.rank } ?: allowed.first()

            // Guess
            potenialWinner == settings.maat -> allowed.first() // Maat kan winnen
            potenialWinner != settings.maat -> allowed.last() // Zetten

            else -> allowed.random()
        }

        return cards.indexOf(suggest)
    }

    private fun generateOpposition(
        cards: List<Card>,
        allowed: List<Card>,
        battles: List<Battle>,
        played: List<Pair<Int, Card>>,
        settings: Setting.Rik
    ): Int {
        val isOpening = battles.lastOrNull()?.isEmpty() ?: true
        val isClosing = battles.lastOrNull()?.isLast() ?: false
        val unknownMaat = settings.maat == null
        val troefPlayed = played.count { it.second.suit == settings.troef }
        val myTroef = cards.count { it.suit == settings.troef }
        val maatTroef = played.filter { it.first == settings.maat }.count { it.second.suit == settings.troef }
        val troefLeft = 13 - troefPlayed - myTroef
        val (potenialWinner, card) = battles.last().highest()
        val suggest : Card? = when {
            // Zetten
            isOpening && unknownMaat -> allowed.firstOrNull { it.suit != settings.mate.suit } ?: allowed.random() // Maat vermijden
            isOpening && troefLeft > 0 ->allowed.firstOrNull { it.suit != settings.troef } ?: allowed.first() // Troef trekken

            // Afmaken
            isClosing && potenialWinner != settings.maat && potenialWinner != settings.player -> allowed.first() // Maat wint
            isClosing && potenialWinner == settings.maat || potenialWinner == settings.player -> allowed.firstOrNull { it.suit == card.suit && it.rank > card.rank } ?: allowed.last()

            // Guess
            potenialWinner != settings.maat && potenialWinner != settings.player -> allowed.first() // Maat kan winnen
            potenialWinner == settings.maat || potenialWinner == settings.player -> allowed.firstOrNull { it.suit == card.suit && it.rank > card.rank } ?: allowed.last()

            else -> allowed.random()
        }

        return cards.indexOf(suggest)
    }
}

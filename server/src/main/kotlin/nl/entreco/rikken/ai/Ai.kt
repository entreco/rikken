package nl.entreco.rikken.ai

import PlayerId
import RikGame
import core.rikken.Actions
import core.rikken.Battle
import core.rikken.Card
import core.rikken.Rank
import core.rikken.RikType
import core.rikken.Setting
import core.rikken.Suit

interface Ai {
    suspend fun generateBid(bids: Map<Int, RikType>, cards: List<Card>): RikType
    suspend fun generateConfirmation(playerId: PlayerId, actions: Actions, cards: List<Card>): RikGame.Event.Confirm
    suspend fun generatePlay(
        playerIndex: Int,
        cards: List<Card>,
        played: List<Pair<Int, Card>>,
        battles: List<Battle>,
        allowed: List<Card>,
        settings: Setting
    ): Int
}

fun List<Card>.suggestTroef(): Suit {
    val grouped = groupBy { it.suit }.maxBy { it.value.sumOf { it.rank.ordinal } }
    return grouped.key
}

fun List<Card>.suggestMate(troef: Suit, excludes: Map<Suit, List<Suit>>): Suit {
    val grouped = groupBy { it.suit }.filter { it.key != troef}
    return grouped.firstNotNullOf { it.key }
}
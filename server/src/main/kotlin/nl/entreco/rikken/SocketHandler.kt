package nl.entreco.rikken

import RikGame
import RikHandler
import io.ktor.websocket.DefaultWebSocketSession

class SocketHandler(
    private val session: DefaultWebSocketSession,
    override val username: String
) : RikHandler {
    override suspend fun chat(chat: RikGame.Event.Chat) {
        session.chat(chat)
    }

    override suspend fun whisper(state: RikGame.State) {
        session.whisper(state)
    }
}
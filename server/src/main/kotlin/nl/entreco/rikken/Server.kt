package nl.entreco.rikken

import Oops
import PlayerId
import RIK_VERSION
import RikGame
import RikGame.Companion.deserialize
import RikGame.None
import RikHandler
import RikServer
import Rikker
import RoomID
import RoomID.Companion.asRoomID
import core.rikken.Actions
import core.rikken.BidManager
import core.rikken.Card
import core.rikken.Rank
import core.rikken.RikSettings
import core.rikken.Round
import core.rikken.ScoreKeeper
import core.rikken.Suit
import core.rikken.sorted
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import io.ktor.server.websocket.webSocket
import io.ktor.websocket.DefaultWebSocketSession
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.delay
import nl.entreco.rikken.ai.Ai
import nl.entreco.rikken.ai.BoringAi
import nl.entreco.rikken.ai.ProdAi
import kotlin.time.times

class KtorServer(
    private val ai: Ai = BoringAi(),
    private val serverSettings: Settings = ServerSettings()
) : RikServer {
    private val original = listOf("Bonske", "Bonus", "Pjotr", "Peerke", "Co", "Coco", "Entreco", "Cootje", "Bassie", "Babel", "vAsch", "vûh Limpt", "Snorrie", "Ron", "El Kango", "vd Sande", "Dielis", "Dieliani", "Rikkert", "Nûh Gouwe", "Leerie", "Dolfke", "Slurpmakers", "Rudi", "Shotta", "Schootje", "Qqluq", "Trotski", "VanRiel", "Roeland", "te Riele", "Gvr")
    private val bots : MutableList<String> = MutableList(original.size){ original[it] }
    private val handlers: MutableList<RikHandler> = mutableListOf()
    internal var state: RikGame.State = RikGame.State.Welcome("empty")
    private lateinit var bidManager: BidManager
    private lateinit var round: Round
    private val scoreKeeper: ScoreKeeper = ScoreKeeper()

    override suspend fun join(sessionHandler: RikHandler, roomId: RoomID) {
        if (!handlers.contains(sessionHandler)) handlers.add(sessionHandler)

        handlers.forEachIndexed { index, h ->
            scoreKeeper.onPlayerJoined(index)
            state = RikGame.State.Waiting(
                index,
                roomId,
                4 - handlers.size
            )
            h.whisper(state)
        }

        if (handlers.size == 4) {
            state = handlers.shuffle(0)
            handlers.forEach { h ->
                h.whisper(state)
            }
        }
    }

    override suspend fun chatAll(message: String) {
        handlers.forEach { h ->
            h.chat(RikGame.Event.Chat(message))
        }
    }

    override suspend fun leave(sessionHandler: RikHandler) {
        scoreKeeper.onPlayerLeft(handlers.indexOf(sessionHandler))
        handlers.remove(sessionHandler)
        state = when (val s = state) {
            is RikGame.State.Welcome -> state
            is RikGame.State.Waiting -> s.copy(
                playersRemaining = s.playersRemaining + 1,
                error = null
            )

            is RikGame.State.Bidding -> state /* TODO */
            is RikGame.State.Selecting -> state /* TODO */
            is RikGame.State.Playing -> s.copy(s.players.map {
                if (it.name == sessionHandler.username) it.copy(name = it.name + "*", bot = true)
                else it
            }, error = null)

            is RikGame.State.Settling -> s.copy(s.players.map {
                if (it.name == sessionHandler.username) it.copy(name = it.name + "*", bot = true)
                else it
            }, error = null)
        }
        handlers.forEach {
            it.whisper(state)
        }
    }

    suspend operator fun plusAssign(event: RikGame.Event) {
        state = when (val s = state) {
            is RikGame.State.Welcome -> s.welcome(event, handlers)
            is RikGame.State.Waiting -> s.waiting(event, handlers)
            is RikGame.State.Bidding -> s.bidding(event, handlers)
            is RikGame.State.Selecting -> s.selecting(event, handlers)
            is RikGame.State.Playing -> s.playing(event, handlers)
            is RikGame.State.Settling -> s.settling(event, handlers)
        }
        handlers.forEach {
            it.whisper(state)
        }

        val a = state
        if (a is RikGame.State.Bidding) postBid(a)
        val b = state
        if (b is RikGame.State.Selecting) postSelect(b)
        val c = state
        if (c is RikGame.State.Playing) postPlay(c)
    }

    private suspend fun postPlay(s: RikGame.State.Playing) {
        if (!round.isNotFinished) {
            delay(3 * serverSettings.simulationTime)
            scoreKeeper.apply { this += round }
            state = RikGame.State.Settling(
                s.players,
                s.hands,
                s.startingPlayer,
                s.settings,
                scoreKeeper.scores
            )
            handlers.forEach {
                it.whisper(state)
            }
        } else {
            val rikker = s.players[round.currentPlayer]
            if (rikker.bot) { // Handle Next Player is a Bot
                val index = ai.generatePlay(
                    rikker.playerIndex,
                    s.hands[rikker.playerIndex],
                    s.played,
                    round.battles(),
                    round.allowedCardsFor(s.hands[rikker.playerIndex]),
                    s.settings
                )
                delay(serverSettings.simulationTime)
                // Submit next move
                this += RikGame.Event.Play(PlayerId(rikker.playerIndex), index)
            }
        }
    }

    private suspend fun postSelect(s: RikGame.State.Selecting) {
        val selector = s.rikSettings.currentPlayer ?: s.startingPlayer
        val actions = s.rikSettings.generateActions()
        if(actions is Actions.ConfirmNoRik){
            this += RikGame.Event.Confirm.NoRik(PlayerId(selector), actions.rikType)
        } else if(actions is Actions.ConfirmMieen) {
            this += RikGame.Event.Confirm.Mieeen(PlayerId(selector))
        } else if (s.players[selector].bot) { // Handle Next Player is a Bot
            // Submit next move
            val event = ai.generateConfirmation(
                PlayerId(selector),
                actions,
                s.hands[selector]
            )
            this += event
        }
    }

    private suspend fun postBid(s: RikGame.State.Bidding) {
        val bids = bidManager.bids()
        if (bidManager.areSettled) {
            state = RikGame.State.Selecting(
                s.players,
                s.hands,
                s.startingPlayer,
                RikSettings(bids, s.hands)
            )
            handlers.forEach { it.whisper(state) }
        } else if (s.players[bidManager.currentPlayer].bot) { // Handle Next Player is a Bot
            // Submit next move
            val nextRikker = bidManager.currentPlayer
            val bid = ai.generateBid(bids, s.hands[nextRikker])
            this += RikGame.Event.Bid(PlayerId(nextRikker), bid)
        }
    }


    private fun RikGame.State.Welcome.welcome(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            else -> this
        }
    }

    private fun RikGame.State.Waiting.waiting(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            is RikGame.Event.Begin -> handlers.shuffle(0)
            else -> this
        }
    }

    private fun RikGame.State.Bidding.bidding(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            is RikGame.Event.Bid -> handlers.bid(this, event)
            else -> this
        }
    }

    private fun RikGame.State.Selecting.selecting(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            is RikGame.Event.Confirm -> handlers.confirm(this, event)
            else -> this
        }
    }

    private fun RikGame.State.Playing.playing(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            is RikGame.Event.Play -> handlers.play(this, event)
            else -> this
        }
    }

    private fun RikGame.State.Settling.settling(
        event: RikGame.Event,
        handlers: MutableList<RikHandler>
    ): RikGame.State {
        return when (event) {
            is RikGame.Event.Begin -> handlers.shuffle((startingPlayer + 1).mod(4), players)
            else -> this
        }
    }

    private fun List<RikHandler>.bid(
        state: RikGame.State.Bidding,
        event: RikGame.Event.Bid
    ): RikGame.State {
        val nextPlayer = state.biddingPlayer
        return if (nextPlayer != event.playerId.index) state.copy(
            error = Oops(
                event.playerId.index,
                "Not your turn buddy"
            )
        )
        else {
            bidManager += event.playerId.index to event.rikType
            state.copy(
                state.players,
                state.hands,
                state.startingPlayer,
                bidManager.currentPlayer,
                bidManager.bids(),
                error = null
            )
        }
    }

    private fun List<RikHandler>.confirm(
        state: RikGame.State.Selecting,
        event: RikGame.Event.Confirm
    ): RikGame.State {
        val currentPlayer = state.rikSettings.currentPlayer ?: state.startingPlayer
        return if (currentPlayer != event.playerId.index) state.copy(
            error = Oops(
                event.playerId.index,
                "Not your turn buddy"
            )
        )
        else {
            round = Round(
                state.rikSettings.create(event),
                state.startingPlayer,
                state.players.map { it.playerIndex })

            RikGame.State.Playing(
                players = state.players,
                hands = state.hands,
                startingPlayer = state.startingPlayer,
                currentPlayer = round.currentPlayer,
                allowed = round.allowedCardsFor(state.hands[round.currentPlayer]),
                winners = emptyList(),
                settings = round.setting,
            )
        }
    }

    private fun List<RikHandler>.play(
        state: RikGame.State.Playing,
        event: RikGame.Event.Play
    ): RikGame.State {
        val currentPlayer = round.currentPlayer
        return if (event.playerId.index != currentPlayer) state.copy(
            error = Oops(
                event.playerId.index,
                "Not your turn Buddy"
            )
        )
        else {
            val card = state.hands[currentPlayer][event.index]
            val hands = state.hands.mapIndexed { index, cards ->
                if (event.playerId.index == index) (state.hands[currentPlayer].take(event.index) + state.hands[currentPlayer].drop(event.index + 1)).sorted()
                else cards.sorted()
            }
            val valid = round.play(currentPlayer, card)
            if (valid) {
                val allowed = round.allowedCardsFor(state.hands[round.currentPlayer])

                // Update state for next player
                state.copy(
                    players = state.players,
                    hands = hands,
                    startingPlayer = state.startingPlayer,
                    currentPlayer = round.currentPlayer,
                    allowed = allowed,
                    settings = state.settings,
                    played = (state.played + (currentPlayer to card)),
                    winners = round.battles().mapNotNull { it.winner() },
                )
            } else state.copy(error = Oops(event.playerId.index, "Invalid move"))
        }
    }

    private fun List<RikHandler>.shuffle(starter: Int, players: List<Rikker> = emptyList()): RikGame.State {
        val deck = Suit.entries.flatMap { s -> Rank.entries.map { r -> Card(r, s) } }
        val hands = deck.shuffled().chunked(13).map { it.sorted() }

        val rikkers = if(players.size == 4) players else {
            val clients = mapIndexed { index, client -> Rikker(index, client.username, false) }
            val bots = (clients.size until 4).map { Rikker(it, bots.pickRandom(), true) }
            clients + bots
        }

        bidManager = BidManager(rikkers.size, starter)
        return RikGame.State.Bidding(
            rikkers,
            hands,
            starter,
            bidManager.currentPlayer,
            bidManager.bids()
        )
    }

    private fun MutableList<String>.pickRandom(): String {
        if(bots.isEmpty()) bots.addAll(original)
        val random = random()
        this.remove(random)
        return random
    }
}

fun Application.configureServer() {
    routing {

        get("/") {
            call.respondText("Rikske... Iemand? $RIK_VERSION")
        }

        val server = KtorServer(ProdAi())

        webSocket("/rikske/{name}/{room}") {

            val username = call.parameters["name"].orEmpty()
            val roomId = call.parameters["room"].asRoomID()
            val sessionHandler = SocketHandler(this, username)
            server.join(sessionHandler, roomId)

            try {
                server.chatAll("$username joined $roomId.")

                for (message in incoming) {
                    message as Frame.Text? ?: continue
                    message.readText().apply {
                        println("Received: $this")

                        when (val event = deserialize()) {
                            is None -> {}
                            is RikGame.Event.Chat -> server.chatAll("[${username}] ${event.msg}")
                            is RikGame.Event -> server += event
                            is RikGame.State -> { /* Server cannot receive state updates, only RikGame.Events send from Clients ... */
                            }
                        }
                    }
                }

            } catch (e: Exception) {
                println("RikServer: " + e.localizedMessage)
                server.chatAll(e.localizedMessage)
            } finally {
                println("Removing $sessionHandler")
                server.leave(sessionHandler)
                server.chatAll("$username left the chat")

                // This means if 1 connections drops, all players return to the Lobby
//                room.broadcast(Waiting(roomId.code, 4 - rooms.size))
            }
        }
    }
}


suspend fun DefaultWebSocketSession.chat(message: RikGame.Event.Chat) {
    send(Frame.Text(message.serialize()))
}

suspend fun DefaultWebSocketSession.whisper(state: RikGame.State) {
    send(Frame.Text(state.serialize()))
}
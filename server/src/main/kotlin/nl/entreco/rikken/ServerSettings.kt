package nl.entreco.rikken

import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

interface Settings{
    val simulationTime: Duration
}
class ServerSettings : Settings {
    override val simulationTime: Duration
        get() = 0.5.seconds
}
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.routing.*
import io.ktor.server.websocket.WebSockets

fun Application.configureSockets() {
    install(Routing) {
        // ...
    }
    install(WebSockets) {
//        pingPeriod = Duration.ofMinutes(1)
//        timeout = Duration.ofMinutes(1)
//        maxFrameSize = Long.MAX_VALUE
//        masking = false
    }
}

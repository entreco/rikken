Change Log
==========


Version 0.0.3 *(2022-04-09)*
----------------------------

* Experiment with Publishing

Version 0.0.2 *(2022-04-09)*
----------------------------

* Initial version

  The Maven coordinates are `com.squareup.retrofit2:adapter-rxjava3`.

  Unlike the RxJava 1 and RxJava 2 adapters, the RxJava 3 adapter's `create()` method will produce asynchronous HTTP requests by default. For synchronous requests use `createSynchronous()` and for synchronous on a scheduler use `createWithScheduler(..)`.


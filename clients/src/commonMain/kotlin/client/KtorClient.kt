package client

import PlayerId
import RIK_VERSION
import RikClient
import RikGame
import RikGame.Companion.deserialize
import RikGame.State.Welcome
import RoomID
import Settings
import core.rikken.RikType
import core.rikken.Suit
import io.ktor.client.HttpClient
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.http.HttpMethod
import io.ktor.http.encodeURLPath
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger

class KtorClient(
    override val username: String,
    override val room: RoomID,
    private val settings: Settings = Settings.Local,
    private val client: HttpClient = HttpClient {
        install(WebSockets) {
            // Customize socket here
        }
    },
) : RikClient {

    private val _events = MutableStateFlow<List<RikGame.Event>>(emptyList())
    override val events: Flow<List<RikGame.Event>> = _events
    private val _state: MutableStateFlow<RikGame.State> = MutableStateFlow(Welcome(RIK_VERSION))
    override val state: Flow<RikGame.State> = _state

    private val myIndex = AtomicInteger(-1)
    override val playerId: PlayerId
        get() = PlayerId(myIndex.get())

    private val send: MutableStateFlow<RikGame.Event?> = MutableStateFlow(null)

    override suspend fun start() {
        client.webSocket(
            method = HttpMethod.Get,
            host = settings.host,
            port = settings.port,
            path = "/rikske/$username/${room.code}".encodeURLPath()
        ) {
            val messageOutputRoutine = launch { outputMessages() }
            val userInputRoutine = launch { inputMessages(send) }

            userInputRoutine.join() // Wait for completion; either "exit" or error
            messageOutputRoutine.cancelAndJoin()
        }
    }

    override fun chat(msg: String) {
        println("FLOW send: $msg")
        send.compareAndSet(send.value, RikGame.Event.Chat(msg))
    }

    override fun begin() {
        send.compareAndSet(send.value, RikGame.Event.Begin(room))
    }

    override fun bid(bid: RikType) {
        send.compareAndSet(send.value, RikGame.Event.Bid(PlayerId(myIndex.get()), bid))
    }

    override fun confirm(troef: Suit?, maat: Suit?, rikType: RikType) {
        val playerId1 = PlayerId(myIndex.get())
        val event : RikGame.Event.Confirm? = when(rikType){
            RikType.Mieen -> RikGame.Event.Confirm.Mieeen(playerId1)
            RikType.Misere,
            RikType.MisereOpen,
            RikType.MiserePraatje,
            RikType.Piek,
            RikType.PiekOpen,
            RikType.PiekPraatje -> RikGame.Event.Confirm.NoRik(playerId1, rikType)
            RikType.Rik,
            RikType.RikBeter -> RikGame.Event.Confirm.Rik(playerId1, troef!!, maat!!, rikType)
            RikType.Solo10,
            RikType.Solo10Beter,
            RikType.Solo11,
            RikType.Solo11Beter,
            RikType.Solo12,
            RikType.Solo12Beter,
            RikType.Solo13,
            RikType.Solo13Beter ->  RikGame.Event.Confirm.Solo(playerId1, troef!!, rikType)
            else -> null
        }
        event?.let {
            send.compareAndSet(send.value, event)
        }
    }

    override fun play(cardIndex: Int) {
        send.compareAndSet(
            send.value, RikGame.Event.Play(PlayerId(myIndex.get()), cardIndex)
        )
    }

    override fun stop() {
        chat("exit")
        client.close()
    }

    private suspend fun DefaultClientWebSocketSession.outputMessages() {
        try {
            for (message in incoming) {
                message as Frame.Text? ?: continue
                message.readText().apply {
                    println("Received: $this")
                    when (val m = deserialize()) {
                        is RikGame.State -> {
                            if (m is RikGame.State.Waiting) myIndex.set(m.index)
                            _state.emit(m)
                        }

                        is RikGame.Event -> _events.emit(_events.value + m)
                        is RikGame.None -> { /* Ignore */
                        }
                    }
                }
            }
        } catch (e: Exception) {
            println("Error while receiving: ${e.message}")
            _events.emit(_events.value + RikGame.Event.Chat(e.localizedMessage))
        }
    }

    private suspend fun DefaultClientWebSocketSession.inputMessages(inputs: Flow<RikGame.Event?>) {
        while (true) {
            println("loop")
            inputs.filterNotNull().collectLatest { event ->
                println("FLOW Collecting latest: $event")
                try {
                    send(Frame.Text(event.serialize()))
                } catch (e: Exception) {
                    println("Error while sending: ${e.message}")
                    _events.emit(_events.value + RikGame.Event.Chat(e.localizedMessage))
                }
            }
        }
    }
}
object Client {
    const val server_url = "ws://127.0.0.1:8080"
    const val emulator = "10.0.2.2"
    const val normal = "127.0.0.1"
    const val physical = "192.168.50.34"
}


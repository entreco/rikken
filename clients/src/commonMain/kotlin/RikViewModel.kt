import RoomID.Companion.asRoomID
import androidx.lifecycle.ViewModel
import client.KtorClient
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class RikViewModel : ViewModel() {
    /**
     * State of this Game
     */
    private val _client = MutableStateFlow(ClientState())
    val client: StateFlow<ClientState> = _client.asStateFlow()

    fun register(username: String, room: String) {
        _client.update { current ->
            current.copy(client = KtorClient(username, room.asRoomID()))
        }
    }

    override fun onCleared() {
        _client.update { current ->
            current.copy(null)
        }
        super.onCleared()
    }
}

data class ClientState(
    val client: RikClient? = null,
)
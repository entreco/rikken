import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

@Composable
fun ErrorDialog(error: Oops?, to: PlayerId) {
    var shouldShowDialog by remember { mutableStateOf(true) }
    if (error != null && error.to == to.index && shouldShowDialog) {
        AlertDialog(
            onDismissRequest = { shouldShowDialog = false },
            confirmButton = {
                TextButton({ shouldShowDialog = false }) {
                    Text("OK")
                }
            },
            title = { Text("Oops") },
            text = { Text(error.message) }
        )
    }
}
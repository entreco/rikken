import RikGame.State.Bidding
import RikGame.State.Playing
import RikGame.State.Selecting
import RikGame.State.Settling
import RikGame.State.Waiting
import RikGame.State.Welcome
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.times
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import auto.AutoSizeText
import cards.CardView
import cards.asPainter
import core.rikken.Actions
import core.rikken.Card
import core.rikken.RikSettings
import core.rikken.RikType
import core.rikken.RikTypeRules
import core.rikken.Score
import core.rikken.Setting
import core.rikken.Suit
import core.rikken.delta
import core.rikken.displayGain
import core.rikken.displayStatus
import core.rikken.isBeter
import org.jetbrains.compose.ui.tooling.preview.Preview
import kotlin.math.abs
import kotlin.math.pow

private val rikYellow = Color(0xFFE3CF15)
private val rikBrown = Color(0xFF7B2828)
private val rikRed = Color(0xFFAE4444)
private val rikGreen = Color(0xFF12B847)
private val rikOpposition = Color(0xFF4A8EBF)
private val rikTeam = Color(0xFF98E397)

@Composable
@Preview
fun App(viewModel: RikViewModel = viewModel { RikViewModel() }) {
    RikTheme {

        Scaffold(topBar = {}) { padding ->

            val state by viewModel.client.collectAsState()
            var screenSize by remember { mutableStateOf(Pair(-1, -1)) }

            Layout(measurePolicy = { m, c ->
                // Use the max width and height from the constraints
                val width = c.maxWidth
                val height = c.maxHeight

                screenSize = Pair(width, height)
                // Measure and place children composables
                val placeables = m.map { measurable ->
                    measurable.measure(c)
                }

                layout(width, height) {
                    var yPosition = 0
                    placeables.forEach { placeable ->
                        placeable.placeRelative(x = 0, y = yPosition)
                        yPosition += placeable.height
                    }
                }
            }, content = {

                Box(
                    Modifier.background(
                        brush = Brush.linearGradient(
                            listOf(
                                Color(0xFF32B950),
                                Color(0xFF146C30)
                            )
                        )
                    ).padding(top = padding.calculateTopPadding())
                ) {
                    state.client?.let {
                        RikScreen(it, screenSize.first, screenSize.second)
                    } ?: RegisterScreen { user, room ->
                        viewModel.register(user, room)
                    }
                }
            })
        }
    }
}

@Composable
fun RegisterScreen(
    modifier: Modifier = Modifier.fillMaxSize().padding(16.dp),
    onRegistered: (String, String) -> Unit
) {
    var username by remember { mutableStateOf("") }
    var room by remember { mutableStateOf("") }
    var registered by remember { mutableStateOf(false) }

    Box(modifier) {
        Column(
            Modifier.width(200.dp).align(Alignment.Center),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            if (registered) {
                Text("Registering...", modifier = Modifier.padding(16.dp))
                CircularProgressIndicator()
            } else {

                RikInput(icon = Icons.Filled.Person, Modifier.fillMaxWidth()) {
                    username = it
                }
                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                ) {
                    RikInput(
                        icon = Icons.Filled.Lock,
                        modifier = Modifier.width(120.dp),
                        max = RoomID.NUM_CHARS
                    ) {
                        room = it
                    }
                    Button(
                        onClick = {
                            onRegistered(username, room)
                            registered = true
                        },
                        colors = ButtonDefaults.buttonColors(),
                        border = BorderStroke(2.dp, Color(0xFF18235B))
                    ) {
                        Text(text = "GO")
                    }
                }
            }
        }

        Text("Version: $RIK_VERSION", color = Color.White, fontSize = 16.sp)
    }
}

@Composable
fun RikScreen(client: RikClient, width: Int, height: Int) {
    val events by client.events.collectAsState(emptyList())
    val state by client.state.collectAsState(Welcome("Nope"))
    val density = LocalDensity.current.density

    LaunchedEffect("init") {
        try {
            client.start()
        } catch (e: Exception) {
            println(e) // TODO: Handle connection error here
        }
    }

    DisposableEffect("deinit") {
        onDispose {
            client.stop()
        }
    }

    if (width > height) {
        val scaleFactor = height / 7 / density
        Row { // Landscape
            RikWindow(scaleFactor.dp, state, client)
            ChatWindow(events.filterIsInstance<RikGame.Event.Chat>(), client)
        }
    } else { // Portrait
        val scaleFactor = width / 7 / density
        Column { // Portrait
            RikWindow(scaleFactor.dp, state, client, false)
            ChatWindow(events.filterIsInstance<RikGame.Event.Chat>(), client)
        }
    }
}

@Composable
private fun RikWindow(
    scaleFactor: Dp,
    state: RikGame.State,
    client: RikClient,
    portrait: Boolean = true,
    modifier: Modifier = Modifier.aspectRatio(1.3f, portrait),
) {
    println("Rik state:$state")
    Box(modifier) {
        when (state) {
            is Welcome -> Text("Version ${state.version}")
            is Waiting -> WaitingScreen(state, client)
            is Bidding -> BiddingScreen(scaleFactor, state, client)
            is Selecting -> SelectingScreen(scaleFactor, state, client)
            is Playing -> PlayingScreen(scaleFactor, state, client)
            is Settling -> SettlingScreen(scaleFactor, state, client)
        }
    }
}


@Composable
fun RikInput(
    icon: ImageVector,
    modifier: Modifier = Modifier,
    max: Int = 32,
    on: (String) -> Unit = {}
) {
    var input by remember { mutableStateOf("") }
    Row(
        modifier
            .background(rikRed, shape = RoundedCornerShape(64.dp))
            .border(4.dp, color = rikBrown, shape = RoundedCornerShape(64.dp))
            .padding(12.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(icon, contentDescription = "", tint = Color.White)
        Spacer(modifier = Modifier.width(8.dp))
        BasicTextField(
            value = input,
            singleLine = true,
            onValueChange = {
                input = it.take(max)
                on(it)
            },
            textStyle = TextStyle.Default.copy(color = Color.White),
        )
    }
}

@Composable
fun BoxScope.WaitingScreen(
    state: Waiting,
    client: RikClient,
    modifier: Modifier = Modifier.align(Alignment.Center),
) {
    state.error?.let {
        Text(it, Modifier.align(Alignment.Center))
    }

    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Waiting for other player(s) to join...")
        Text(
            "${state.playersRemaining}",
            fontSize = 100.sp,
            color = Color.White,
            modifier = Modifier.padding(16.dp).border(width = 8.dp, rikBrown, CircleShape)
                .background(rikRed, CircleShape).size(200.dp),
            textAlign = TextAlign.Center,
            lineHeight = 160.sp
        )

        Button(modifier = Modifier.defaultMinSize(60.dp, 60.dp), onClick = {
            client.begin()
        }) {
            Text("Start with CPU")
        }
    }
    Column(Modifier.align(Alignment.BottomStart)) {
        Text(client.username)
        Text(state.code.code)
    }
}

@Composable
fun BoxScope.BiddingScreen(spread: Dp, state: Bidding, client: RikClient) {
    ErrorDialog(state.error, client.playerId)

    val left by derivedStateOf { state.players.leftOf(client.playerId) to state.bids.leftOf(client.playerId) }
    val top by derivedStateOf { state.players.topOf(client.playerId) to state.bids.topOf(client.playerId) }
    val right by derivedStateOf {
        state.players.rightOf(client.playerId) to state.bids.rightOf(
            client.playerId
        )
    }

    // Cards on all sides
    PlayersAndCards(
        spread,
        state.hands,
        client.playerId.index,
        top = {
            Opponent(
                top.first.named(),
                top.second.thinking(),
                isYourTurn = state.biddingPlayer == top.first.playerIndex,
                modifier = Modifier.align(Alignment.TopCenter).wrapContentHeight()
            )
        },
        left = {
            Opponent(
                left.first.named(),
                left.second.thinking(),
                isYourTurn = state.biddingPlayer == left.first.playerIndex,
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopStart)
                    .wrapContentHeight()
            )
        },
        right = {
            Opponent(
                right.first.named(),
                right.second.thinking(),
                isYourTurn = state.biddingPlayer == right.first.playerIndex,
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopEnd)
                    .wrapContentHeight()
            )
        },
        down = {},
        centre = {
            // Show BidCentre if it is My Turn
            if (state.biddingPlayer == client.playerId.index) {
                BidCentre(state.bids, client)
            } else {
                Text("Waiting for ${state.players[state.biddingPlayer].named()}")
            }
        }
    )
}

private fun String.thinking(): String = if (this == "-") "-" else "☝\uFE0F $this"

@Composable
fun BoxScope.SelectingScreen(spread: Dp, state: Selecting, client: RikClient) {
    ErrorDialog(state.error, client.playerId)

    val left by remember { mutableStateOf(state.players.leftOf(client.playerId)) }
    val top by remember { mutableStateOf(state.players.topOf(client.playerId)) }
    val right by remember { mutableStateOf(state.players.rightOf(client.playerId)) }

    // Cards on all sides
    PlayersAndCards(
        spread,
        state.hands,
        client.playerId.index,
        top = {
            Opponent(
                top.named(),
                subtitle = if(state.rikSettings.currentPlayer == top.playerIndex) "\uD83E\uDD14" else "⌛",
                modifier = Modifier.align(Alignment.TopCenter))
        },
        left = {
            Opponent(
                left.named(),
                subtitle = if(state.rikSettings.currentPlayer == left.playerIndex) "\uD83E\uDD14" else "⌛",
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopStart)
            )
        },
        right = {
            Opponent(
                right.named(),
                subtitle = if(state.rikSettings.currentPlayer == right.playerIndex) "\uD83E\uDD14" else "⌛",
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopEnd)
            )
        },
        down = {
            Text("${client.username} - TODO: Current Selection action?")
        },
        centre = {
            if (state.rikSettings.currentPlayer == client.playerId.index) {
                SelectCentre(spread, state.rikSettings, state.hands[client.playerId.index], client)
            } else {
                Text("Waiting for ${state.players.firstOrNull { it.playerIndex == state.rikSettings.currentPlayer }}")
            }
        }
    )
}

@Composable
fun BoxScope.PlayingScreen(spread: Dp, state: Playing, client: RikClient) {
    ErrorDialog(state.error, client.playerId)

    val left by derivedStateOf {
        state.players.leftOf(client.playerId) to state.winners.leftOf(
            client.playerId
        )
    }
    val top by derivedStateOf { state.players.topOf(client.playerId) to state.winners.topOf(client.playerId) }
    val right by derivedStateOf {
        state.players.rightOf(client.playerId) to state.winners.rightOf(
            client.playerId
        )
    }
    val me by derivedStateOf { state.players[client.playerId.index] to state.winners.count { it == client.playerId.index } }

    // Cards on all sides
    PlayersAndCards(
        spread,
        state.hands,
        client.playerId.index,
        state.allowed,
        onPlay = { client.play(it) },
        top = {
            Opponent(
                top.first.named(),
                top.first.playerIndex.statusOf(state.settings),
                state.settings,
                state.currentPlayer == top.first.playerIndex,
                Modifier.align(Alignment.TopCenter).wrapContentHeight(),
            ) {
                RikBolletje(spread, top.first, state.winners, state.settings, it)
            }
        },
        left = {
            Opponent(
                left.first.named(),
                left.first.playerIndex.statusOf(state.settings),
                state.settings,
                state.currentPlayer == left.first.playerIndex,
                Modifier.padding(vertical = spread).align(Alignment.TopStart).wrapContentHeight(),
            ) {
                RikBolletje(spread, left.first, state.winners, state.settings, it)
            }
        },
        right = {
            Opponent(
                right.first.named(),
                right.first.playerIndex.statusOf(state.settings),
                state.settings,
                state.currentPlayer == right.first.playerIndex,
                Modifier.padding(vertical = spread).align(Alignment.TopEnd).wrapContentHeight(),
            ) {
                RikBolletje(spread, right.first, state.winners, state.settings, it)
            }
        },
        down = {

            PlayerView(
                client.username,
                client.playerId.index.statusOf(state.settings),
                state.settings,
                Modifier.align(Alignment.BottomCenter).wrapContentHeight(),
                state.currentPlayer == client.playerId.index,
            ) {
                RikBolletje(spread, me.first, state.winners, state.settings, it)
            }
        },
        centre = {
            TableCentre(spread, state.played, client)
        }
    )
}

@Composable
private fun RikBolletje(
    spread: Dp,
    me: Rikker,
    winners: List<Int>,
    settings: Setting,
    modifier: Modifier
) {
    val myBattles by derivedStateOf { winners.count { it == me.playerIndex } }
//    val rikBattles = if (settings is Setting.Rik) winners.count { it == settings.player } else 0
//    val maatBattles = if (settings is Setting.Rik) winners.count { it == settings.maat } else 0
//    val loozers = if (settings is Setting.Rik) winners.count { it != settings.player && it != settings.maat } else 0

    val badge by derivedStateOf {
        when (settings) {
            is Setting.Rik -> if (settings.players().contains(me.playerIndex)) {
                myBattles + if (settings.maat != null) winners.count { it == settings.maat } else 0
            } else if (settings.maat == me.playerIndex) {
                myBattles + winners.count { it == settings.player }
            } else {
                myBattles + if (settings.maat != null) winners.count { it != settings.maat && it != me.playerIndex && it != settings.player } else 0
            }

            is Setting.Solo -> if (settings.players().contains(me.playerIndex)) {
                winners.count { it != me.playerIndex }
            } else myBattles

            else -> myBattles
        }
    }

    val color by derivedStateOf {
        when (settings) {
            is Setting.Rik -> if (settings.players().contains(me.playerIndex)) {
                rikTeam
            } else if (settings.maat == me.playerIndex) {
                rikTeam
            } else {
                rikOpposition
            }

            is Setting.Solo -> if (settings.players()
                    .contains(me.playerIndex)
            ) rikTeam else rikOpposition

            else -> rikOpposition
        }
    }

    Box(
        modifier = modifier.size(spread / 3).padding(4.dp)
            .background(color = color, shape = CircleShape),
        contentAlignment = Alignment.Center
    ) {
        AutoSizeText(
            text = "$badge",
            color = Color.White,
        )
    }
}

@Composable
private fun BoxScope.PlayersAndCards(
    spread: Dp,
    hands: List<List<Card>>,
    clientIndex: Int,
    allowed: List<Card> = emptyList(),
    onPlay: (Int) -> Unit = {},
    showOpponentCards: Boolean = false,
    top: @Composable BoxScope.() -> Unit,
    left: @Composable BoxScope.() -> Unit,
    right: @Composable BoxScope.() -> Unit,
    down: @Composable BoxScope.() -> Unit,
    centre: @Composable BoxScope.() -> Unit,
) {

    Box(
        modifier = Modifier
            .fillMaxHeight(0.25f)
            .fillMaxWidth()
            .align(Alignment.TopEnd),
        contentAlignment = Alignment.TopCenter,
    ) {
        OpponentDeck(
            spread,
            hands[(clientIndex + 1).mod(4)],
            Alignment.TopCenter,
            Modifier.offset(0.dp, -0.2 * spread).rotate(180f),
            showOpponentCards
        )
        top()
    }

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth(0.25f),
        contentAlignment = Alignment.CenterStart,
    ) {
        OpponentDeck(
            spread,
            hands[(clientIndex + 2).mod(4)],
            Alignment.CenterStart,
            Modifier.offset(-0.6 * spread, 0.dp).rotate(90f),
            showOpponentCards
        )
        left()
    }

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth(0.25f)
            .align(Alignment.BottomEnd),
        contentAlignment = Alignment.CenterEnd,
    ) {
        OpponentDeck(
            spread,
            hands[(clientIndex + 3).mod(4)],
            Alignment.CenterEnd,
            Modifier.offset(0.6 * spread, 0.dp).rotate(270f),
            showOpponentCards
        )
        right()
    }

    Box(
        modifier = Modifier
            .fillMaxHeight(0.25f)
            .fillMaxWidth()
            .align(Alignment.BottomStart),
        contentAlignment = Alignment.BottomCenter,
    ) {
        PlayerDeck(
            spread,
            hands[clientIndex],
            allowed
        ) { onPlay(it) }

        down()
    }

    Box(
        modifier = Modifier
            .fillMaxSize(0.5f)
            .align(Alignment.Center), contentAlignment = Alignment.Center
    ) {
        centre()
    }
}

@Composable
private fun Opponent(
    name: String,
    subtitle: String = "",
    settings: Setting? = null,
    isYourTurn: Boolean = false,
    modifier: Modifier = Modifier,
    badge: @Composable (Modifier) -> Unit = {}
) {
    PlayerView(name, subtitle, settings, modifier, isYourTurn, badge)
}

@Composable
fun PlayerDeck(
    spread: Dp,
    cards: List<Card>,
    allowed: List<Card>,
    onPlay: (Int) -> Unit,
) {
    Deck(cards, spread, rotation = 5f, y = 2.dp, allowed = allowed) { index ->
        onPlay(index)
    }
}


@Composable
private fun OpponentDeck(
    spread: Dp,
    cards: List<Card>,
    alignment: Alignment,
    modifier: Modifier,
    showCards: Boolean,
) {
    val opponentSpread by derivedStateOf { 0.8 * spread }
    Box(modifier, contentAlignment = alignment) {
        Deck(cards, opponentSpread, rotation = 3f, y = 1.dp, shown = showCards, clickable = false)
    }
}

@Composable
private fun Deck(
    cards: List<Card>,
    spread: Dp,
    y: Dp = 2.dp,
    rotation: Float = 5f,
    shown: Boolean = true,
    clickable: Boolean = true,
    allowed: List<Card> = emptyList(),
    onPlay: (Int) -> Unit = {}
) {
    var selectedCard by remember { mutableStateOf<Card?>(null) }
    Box {
        cards.mapIndexed { index, card ->

            // TODO: Extract this duplication into reusable Composable

            val middle by derivedStateOf { index - (cards.size / 2) }
            val centre by derivedStateOf { abs(index - cards.size / 2).toDouble().pow(2).toInt() }

            CardView(
                card = card,
                modifier = Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex(index * 1f)
                    .offset(0.4 * spread * middle, y * centre)
                    .rotate(rotation * middle),
                yOffset = if (card == selectedCard) -0.3 * spread else 0.dp,
                shown = shown,
                clickable = clickable,
                onClick = {
                    if (selectedCard != card && allowed.contains(it)) selectedCard = it
                    else if (allowed.contains(it)) onPlay(index)
                }
            )
        }
    }
}

@Composable
private fun BoxScope.BidCentre(
    bids: Map<Int, RikType>,
    client: RikClient,
    modifier: Modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally)
        .align(Alignment.Center).background(Color(0x48FFFFFF), shape = RoundedCornerShape(6.dp))
        .padding(8.dp),
) {
    var selectedOption by remember { mutableStateOf<RikType?>(null) }
    val options by derivedStateOf { RikTypeRules.allowed(bids) }

    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text("Wat zou je doen?")
        LazyVerticalGrid(
            columns = GridCells.Fixed(4),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalArrangement = Arrangement.SpaceEvenly,
        ) {
            items(options) { option ->
                TextButton(
                    { selectedOption = option },
                    enabled = selectedOption != option,
                    modifier = Modifier.borderWhenSelected { selectedOption == option }) {
                    Text(option.desc)
                }
            }
        }
        Button(onClick = { client.bid(selectedOption!!) }, enabled = selectedOption != null) {
            Text("GO")
        }
    }
}

@Composable
private fun BoxScope.SelectCentre(
    spread: Dp,
    settings: RikSettings,
    cards: List<Card>,
    client: RikClient,
    modifier: Modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally)
        .align(Alignment.Center).background(Color(0x48FFFFFF), shape = RoundedCornerShape(6.dp))
        .padding(8.dp),
) {
    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(16.dp))
    {
        when (val actions = settings.generateActions()) {
            is Actions.ConfirmRik.Normal -> SelectNormal(
                spread,
                actions.rikType,
                actions.excludes
            ) { troef, maat ->
                client.confirm(troef, maat, actions.rikType)
            }

            is Actions.ConfirmNoRik -> client.confirm(null, null, actions.rikType)
            is Actions.ConfirmRik.Solo -> SelectSolo(spread, actions.rikType) { troef ->
                client.confirm(troef, null, actions.rikType)
            }

            is Actions.ConfirmMieen -> client.confirm(null, null, RikType.Mieen)
        }
    }
}

@Composable
fun SelectNormal(
    spread: Dp,
    rikType: RikType,
    excludes: Map<Suit, List<Suit>>,
    onSelected: (troef: Suit, maat: Suit) -> Unit
) {
    var troef by remember { mutableStateOf<Suit?>(if (rikType.isBeter) Suit.HEARTS else null) }
    var maat by remember { mutableStateOf<Suit?>(null) }

    SuitSelector(
        spread,
        "Select Troef:",
        troef,
        excludes = if (rikType.isBeter) listOf(
            Suit.SPADES,
            Suit.CLUBS,
            Suit.DIAMONDS
        ) else emptyList()
    ) { troef = it; maat = null }

    val maatExcludes: List<Suit> =
        troef?.let { listOfNotNull(troef) + excludes.getOrElse(it) { emptyList() } } ?: emptyList()
    SuitSelector(spread, "Select Maat:", maat, excludes = maatExcludes) {
        maat = it
    }

    Button({ onSelected(troef!!, maat!!) }, enabled = troef != null && maat != null) {
        Text("Confirm")
    }
}

@Composable
private fun SuitSelector(
    spread: Dp,
    text: String,
    selectedSuit: Suit?,
    excludes: List<Suit> = emptyList(),
    onSelected: (troef: Suit) -> Unit = {}
) {
    Text(text)
    Row(horizontalArrangement = Arrangement.SpaceBetween) {
        SelectionCard(spread, Suit.CLUBS, selectedSuit, excludes) {
            onSelected(it)
        }
        SelectionCard(spread, Suit.DIAMONDS, selectedSuit, excludes) {
            onSelected(it)
        }
        SelectionCard(spread, Suit.SPADES, selectedSuit, excludes) {
            onSelected(it)
        }
        SelectionCard(spread, Suit.HEARTS, selectedSuit, excludes) {
            onSelected(it)
        }
    }
}

@Composable
private fun SelectionCard(
    spread: Dp,
    suit: Suit,
    selectedSuit: Suit?,
    excludes: List<Suit>,
    onSelected: (troef: Suit) -> Unit
) {
    TextButton(
        onClick = { onSelected(suit) },
        modifier = Modifier.size(spread / 1.5f).borderWhenSelected { selectedSuit == suit }
            .alpha(if (!excludes.contains(suit)) 1f else 0.2f),
        enabled = !excludes.contains(suit)
    ) {
        Image(
            painter = suit.asPainter(),
            contentDescription = "",
            contentScale = ContentScale.Fit,
        )
    }
}

private fun Modifier.borderWhenSelected(condition: () -> Boolean): Modifier {
    val modifier = if (condition()) Modifier.border(4.dp, rikYellow, CircleShape) else Modifier
    return this.then(modifier)
}

@Composable
fun SelectSolo(spread: Dp, rikType: RikType, onSelected: (troef: Suit) -> Unit) {
    if (rikType.isBeter) {
        onSelected(Suit.HEARTS)
        return
    }

    var troef by remember { mutableStateOf<Suit?>(if (rikType.isBeter) Suit.HEARTS else null) }
    SuitSelector(
        spread,
        "Select Troef:",
        troef,
        excludes = if (rikType.isBeter) listOf(
            Suit.SPADES,
            Suit.CLUBS,
            Suit.DIAMONDS
        ) else emptyList()
    ) { troef = it }
    Button({ onSelected(troef!!) }, enabled = troef != null) {
        Text("Confirm")
    }
}

@Composable
private fun BoxScope.TableCentre(
    spread: Dp,
    played: List<Pair<Int, Card>>,
    client: RikClient,
    modifier: Modifier = Modifier
        .fillMaxSize(0.5f)
        .align(Alignment.Center),
) {
    Box(
        modifier = modifier, contentAlignment = Alignment.Center
    ) {

        // Border
        Box(Modifier.fillMaxSize().border(width = 10.dp, shape = CircleShape, color = rikGreen))

//        Me: 3-1 %
//        Index: 0
//        Also need to correct for StartingPlayer Shift

        played.chunked(4).lastOrNull()?.forEachIndexed { index, card ->

//            val modulo = (index - client.playerId.index + currentPlayerOffset).mod(4)
//            println("POSITIONS: me:${client.playerId} mod:$modulo currentPlayer:$currentPlayerOffset card:$index")

            val mod = when ((card.first - client.playerId.index).mod(4)) {
                0 -> Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex(6f)
                    .offset(0.1 * spread, 0.6 * spread) // Down
                1 -> Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex(1f)
                    .offset(-0.56 * spread, 0.2 * spread) // Left
                2 -> Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex(0f)
                    .offset(-0.1 * spread, -0.6 * spread) // Up
                3 -> Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex(4f)
                    .offset(0.56 * spread, -0.2 * spread) // Right
                else -> Modifier
            }

            CardView(card.second, mod, true)
        }
    }
}

@Composable
fun RikSettingsView(
    players: List<Rikker>,
    settings: Setting,
    winners: List<Int>,
    me: PlayerId,
    modifier: Modifier = Modifier.wrapContentWidth(Alignment.CenterHorizontally)
        .background(rikRed, shape = RoundedCornerShape(6.dp)).padding(8.dp)
) {
    Column(modifier = modifier) {
        val game = when (settings) {
            is Setting.Rik -> "Rik (${settings.goal}) - ${players[settings.player].named()} ${
                settings.maat?.let {
                    players.getOrElse(it) { null }?.name.orEmpty()
                } ?: ""
            }"

            is Setting.Solo -> "Rik (${settings.goal}) - ${players[settings.player].named()}"
            is Setting.NoRik -> "Piek (${settings.piekers.joinToString { players[it].name }}) / Misère (${settings.miserders.joinToString { players[it].name }})"
            else -> "Ronde"
        }
        val roundsPlayed = winners.size + 1
        val title = if (roundsPlayed <= 13) {
            "${roundsPlayed}/13 | $game"
        } else {
            "Finished"
        }

        val troef = when (settings) {
            is Setting.Rik -> settings.troef
            is Setting.Solo -> settings.troef
            else -> ""
        }
        val maat = when (settings) {
            is Setting.Rik -> settings.mate.suit
            else -> ""
        }
        val summary = when (settings) {
            is Setting.Rik -> "  $troef met $maat   "
            is Setting.Solo -> "  $troef troef   "
            is Setting.NoRik -> ""
            is Setting.Mieeen -> "Mieën :("
            else -> "-"
        }

        val header = "$title $summary "
        Text(header)
    }
}

@Composable
fun PlayerView(
    name: String,
    status: String,
    setting: Setting? = null,
    modifier: Modifier = Modifier,
    isYourTurn: Boolean = false,
    badge: @Composable (Modifier) -> Unit = {}
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        modifier = modifier.then(
            Modifier.padding(8.dp)
                .background(rikRed, shape = CircleShape)
                .border(4.dp, color = if (isYourTurn) rikYellow else rikBrown, shape = CircleShape)
                .padding(4.dp)
        )
    ) {
        badge(Modifier.wrapContentHeight())
        Column(
            verticalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.padding(horizontal = 16.dp)
        ) {
            Text(
                if (isYourTurn) "$name \uD83E\uDD14" else name,
                color = Color.White,
                fontSize = 20.sp
            )
            Row(horizontalArrangement = Arrangement.spacedBy(4.dp), verticalAlignment = Alignment.CenterVertically) {
                Text(status, color = Color.White, fontSize = 12.sp, textAlign = TextAlign.Center)
                if (status != "-" && setting is Setting.Rik) {
                    Image(painter = setting.troef.asPainter(), contentDescription = "Troef",modifier = Modifier.size(14.dp))
                    Text("met", color = Color.White, fontSize = 12.sp, textAlign = TextAlign.Center)
                    Image(
                        painter = setting.mate.suit.asPainter(),
                        contentDescription = "Maat",
                        modifier = Modifier.size(14.dp)
                    )
                    Text(setting.mate.rank.name, color = Color.White, fontSize = 12.sp, textAlign = TextAlign.Center)

                } else if (status != "-" && setting is Setting.Solo) {
                    Image(
                        painter = setting.troef.asPainter(),
                        contentDescription = "Troef",
                        modifier = Modifier.size(14.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun BoxScope.SettlingScreen(
    spread: Dp,
    state: Settling,
    client: RikClient,
) {
    ErrorDialog(state.error, client.playerId)

    val left by derivedStateOf { state.players.leftOf(client.playerId) to state.scores.leftOf(client.playerId) }
    val top by derivedStateOf { state.players.topOf(client.playerId) to state.scores.topOf(client.playerId) }
    val right by derivedStateOf {
        state.players.rightOf(client.playerId) to state.scores.rightOf(
            client.playerId
        )
    }

    // Cards on all sides
    PlayersAndCards(
        spread,
        state.hands,
        client.playerId.index,
        showOpponentCards = true,
        top = {
            Opponent(
                top.first.named(),
                top.second.displayStatus(),
                modifier = Modifier.align(Alignment.TopCenter)
            ) {
                GainedView(spread, top.second, it)
            }
        },
        left = {
            Opponent(
                left.first.named(),
                left.second.displayStatus(),
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopStart)
            ) {
                GainedView(spread, left.second, it)
            }
        },
        right = {
            Opponent(
                right.first.named(),
                right.second.displayStatus(),
                modifier = Modifier.padding(vertical = spread).align(Alignment.TopEnd)
            ) {
                GainedView(spread, right.second, it)
            }
        },
        down = {
            val score = state.scores[client.playerId.index]
            PlayerView(
                client.username,
                score.displayStatus(),
                null,
                Modifier.align(Alignment.BottomCenter)
            ) {
                GainedView(spread, score ?: Score(), it)
            }
        },
        centre = {

            // SettleCentre
//            TableCentre(spread, state.played, client)

            Button({ client.begin() }) {
                Text("Go again")
            }
        }
    )
}

@Composable
private fun GainedView(spread: Dp, score: Score, modifier: Modifier) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .size(spread / 3)
            .padding(16.dp)
            .background(color = rikTeam, shape = CircleShape),
    ) {
        val d = if (score.delta > 0) "▲" else "▼"
        val c = if (score.delta > 0) Color.Green else Color.Red

        Text(
            (if (score.delta > 0) "+" else "") + score.displayGain(),
            color = Color.White,
            fontSize = 20.sp,
        )
        Text(d, color = c, fontSize = 18.sp)
    }
}

@Composable
private fun ChatWindow(
    messages: List<RikGame.Event.Chat>,
    client: RikClient,
    modifier: Modifier = Modifier.fillMaxSize(),
) {
    val state = rememberLazyListState()
    var input by remember { mutableStateOf("") }

    LaunchedEffect(messages.size) {
        state.scrollToItem(messages.size)
    }

    Column(modifier.padding(6.dp)) {

        LazyColumn(
            Modifier.weight(1f).fillMaxWidth()
                .background(Color(0x48FFFFFF), shape = RoundedCornerShape(6.dp)).padding(6.dp),
            verticalArrangement = Arrangement.Bottom,
            state = state
        ) {
            items(messages) {
                Text(it.msg, Modifier.fillMaxWidth())
            }
        }

        Spacer(Modifier.height(6.dp))

        Row(Modifier.wrapContentHeight(), horizontalArrangement = Arrangement.SpaceBetween) {
            TextField(value = input, { input = it }, modifier = Modifier.weight(2f).onKeyEvent {
                when (it.key) {
                    Key.Enter, Key.NumPadEnter -> {
                        client.chat(input).also { input = "" }
                        true
                    }

                    else -> false
                }
            }, singleLine = true)
            TextButton(modifier = Modifier.defaultMinSize(60.dp, 60.dp), onClick = {
                client.chat(input).also { input = "" }
            }) {
                Text("Send")
            }
        }
    }
}

private fun List<Rikker>.leftOf(index: PlayerId) = rikker(index, 1)
private fun List<Int>.leftOf(index: PlayerId) = battles(index, 1)
private fun Setting.leftOf(index: PlayerId) = goal(index, 1)
private fun Map<Int, RikType>.leftOf(index: PlayerId): String = bid(index, 1).desc
private fun Map<Int, Score>.leftOf(index: PlayerId): Score = score(index, 1)

private fun List<Rikker>.topOf(index: PlayerId) = rikker(index, 2)
private fun List<Int>.topOf(index: PlayerId) = battles(index, 2)
private fun Setting.topOf(index: PlayerId) = goal(index, 2)
private fun Map<Int, RikType>.topOf(index: PlayerId): String = bid(index, 2).desc
private fun Map<Int, Score>.topOf(index: PlayerId): Score = score(index, 2)

private fun List<Rikker>.rightOf(index: PlayerId) = rikker(index, 3)
private fun List<Int>.rightOf(index: PlayerId) = battles(index, 3)
private fun Setting.rightOf(index: PlayerId) = goal(index, 3)
private fun Map<Int, RikType>.rightOf(index: PlayerId): String = bid(index, 3).desc
private fun Map<Int, Score>.rightOf(index: PlayerId): Score = score(index, 3)

private fun Setting.me(index: PlayerId) = goal(index, 0)

private fun List<Rikker>.rikker(index: PlayerId, offset: Int) =
    this[(index.index + offset).mod(4)]

private fun Setting.goal(index: PlayerId, offset: Int): Int =
    when (this) {
        is Setting.Rik -> if (players().contains((index.index + offset).mod(4))) goal else 14 - goal
        is Setting.Solo -> if (players().contains((index.index + offset).mod(4))) goal else 14 - goal
        is Setting.NoRik -> if (piekers.contains((index.index + offset).mod(4))) 1 else if (miserders.contains(
                (index.index + offset).mod(4)
            )
        ) 0 else 0

        is Setting.Invalid -> 0
        is Setting.Mieeen -> 0
    }


private fun List<Int>.battles(index: PlayerId, offset: Int) =
    with((index.index + offset).mod(4)) {
        count { it == this }
    }

private fun Map<Int, Score>.score(index: PlayerId, offset: Int): Score =
    getOrElse((index.index + offset).mod(4)) { Score() }

private fun Map<Int, RikType>.bid(index: PlayerId, offset: Int): RikType =
    getOrElse((index.index + offset).mod(4)) { RikType.None }

private fun Int.statusOf(settings: Setting): String = when (settings) {
    is Setting.Mieeen -> RikType.Mieen.desc
    is Setting.NoRik -> if (settings.players().contains(this)) settings.describe() else "-"
    is Setting.Rik -> if (settings.players().contains(this)) settings.describe() else "-"
    is Setting.Solo -> if (settings.players().contains(this)) settings.describe() else "-"
    else -> ""
}
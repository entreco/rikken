import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
fun RikTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colorScheme = MaterialTheme.colorScheme,
        typography = MaterialTheme.typography,
        content = content
    )
}
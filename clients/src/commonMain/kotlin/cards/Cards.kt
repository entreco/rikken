package cards

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageShader
import androidx.compose.ui.graphics.ShaderBrush
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import auto.AutoSizeText
import core.rikken.Card
import core.rikken.Suit
import org.jetbrains.compose.resources.imageResource
import org.jetbrains.compose.resources.painterResource
import rikken.clients.generated.resources.Res
import rikken.clients.generated.resources.clubs
import rikken.clients.generated.resources.diamonds
import rikken.clients.generated.resources.hearts
import rikken.clients.generated.resources.pattern
import rikken.clients.generated.resources.spades


@Composable
fun CardView(card: Card, modifier: Modifier = Modifier, shown: Boolean = false, clickable: Boolean = false, yOffset: Dp = 0.dp, onClick: (Card)-> Unit = {}) {
    Box(
        modifier = modifier
            .offset(0.dp, yOffset)
            .shadow(4.dp)
            .background(Color.White, shape = RoundedCornerShape(16.dp))
            .padding(8.dp)
//            .aspectRatio(0.55f)
            .clickable { if(clickable) onClick(card) },
        contentAlignment = Alignment.Center,
    ) {
        if (shown) {
            Number(card)
            Graphic(card)
        } else {
            Back()
        }
    }
}

@Composable
fun BoxScope.Number(
    card: Card,
    modifier: Modifier = Modifier
        .fillMaxWidth(0.25f)
        .fillMaxHeight(0.30f)
        .align(Alignment.TopStart)
) {
    Column(modifier) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.5f),
            contentAlignment = Alignment.Center
        ) {
            AutoSizeText(text = card.rank.toString())
        }
        Image(
            painter = card.suit.asPainter(),
            contentScale = ContentScale.Fit,
            contentDescription = "",
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Composable
fun BoxScope.Graphic(
    card: Card,
    modifier: Modifier = Modifier
        .fillMaxWidth(0.90f)
        .fillMaxHeight(0.66f)
        .align(Alignment.BottomEnd)
) {
    Image(
        painter = card.suit.asPainter(),
        contentDescription = "",
        contentScale = ContentScale.Fit,
        modifier = modifier
    )
}

@Composable
fun BoxScope.Back() {
    val image = imageResource(Res.drawable.pattern)
    val brush = remember(image) { ShaderBrush(ImageShader(image, TileMode.Repeated, TileMode.Repeated)) }
    Box(Modifier.fillMaxSize().background(brush))
}

@Composable
fun Suit.asPainter() = painterResource(
    when (this) {
        Suit.HEARTS -> Res.drawable.hearts
        Suit.CLUBS -> Res.drawable.clubs
        Suit.DIAMONDS -> Res.drawable.diamonds
        Suit.SPADES -> Res.drawable.spades
    }
)
package nl.entreco.rikken

import PlayerId
import PlayerView
import RegisterScreen
import RikClient
import RikGame
import RoomID
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.times
import androidx.compose.ui.zIndex
import cards.CardView
import core.rikken.Card
import core.rikken.Rank
import core.rikken.RikType
import core.rikken.Suit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlin.math.abs
import kotlin.math.pow

//@Preview
@Composable
fun CardPreview() {
    val card = Card(Rank.QUEEN, Suit.HEARTS)
    CardView(card = card)
}

//@Preview
@Composable
fun WelcomePreview() {
    val card = Card(Rank.QUEEN, Suit.HEARTS)
    Box {
        RegisterScreen(Modifier.size(200.dp, 100.dp), { _, _ -> })
    }
}

//@Preview
@Composable
fun PlayerPreview() {
    MaterialTheme(
        colorScheme = MaterialTheme.colorScheme,
        typography = MaterialTheme.typography,
    ) {
        PlayerView("Coco", "Subtitle")
    }
}


@Preview(widthDp = 800, heightDp = 906)
@Composable
fun LayoutExperiment() {
    val density = LocalDensity.current.density
    var screenSize by remember { mutableStateOf(Pair(-1, -1)) }
    Layout(measurePolicy = { m, c ->
        // Use the max width and height from the constraints
        val width = c.maxWidth
        val height = c.maxHeight

        screenSize = Pair(width, height)
        // Measure and place children composables
        val placeables = m.map { measurable ->
            measurable.measure(c)
        }

        layout(width, height) {
            var yPosition = 0
            placeables.forEach { placeable ->
                placeable.placeRelative(x = 0, y = yPosition)
                yPosition += placeable.height
            }
        }
    }, content = {
        val cards = listOf(Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS),Card(Rank.QUEEN, Suit.HEARTS))
        if (screenSize.first > screenSize.second) {
            val spread = screenSize.second / 7
            Row(
                Modifier.background(
                    brush = Brush.linearGradient(
                        listOf(
                            Color(0xFF32B950),
                            Color(0xFF146C30)
                        )
                    )
                )
            ) { // Landscape
                Rik((spread / density).dp, cards, emptyList(), {})
                Chat()
            }
        } else { // Portrait
            val spread = screenSize.first / 7
            Column(
                Modifier.background(
                    brush = Brush.linearGradient(
                        listOf(
                            Color(0xFF32B950),
                            Color(0xFF146C30)
                        )
                    )
                )
            ) {
                Rik((spread / density).dp, cards, emptyList(), {})
                Chat()
            }
        }
    })
}

@Composable
private fun Chat() {
    Box(
        Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        Text(text = "CHat")
    }
}

@Composable
private fun Rik(spread: Dp, cards: List<Card>, allowed: List<Card>, onPlay: (Int) -> Unit) {

    Box(Modifier.aspectRatio(1f)) {
        Box(
            modifier = Modifier
                .fillMaxHeight(0.25f)
                .fillMaxWidth()
                .align(Alignment.TopEnd),
            contentAlignment = Alignment.TopCenter
        ) { Text(text = "Top") }

        Box(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(0.25f),
            contentAlignment = Alignment.CenterStart
        ) { Text(text = "Left") }


        Box(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(0.25f)
                .align(Alignment.BottomEnd),
            contentAlignment = Alignment.CenterEnd
        ) { Text(text = "Right") }

        Box(
            modifier = Modifier
                .fillMaxHeight(0.25f)
                .fillMaxWidth()
                .align(Alignment.BottomStart),
            contentAlignment = Alignment.BottomCenter
        ) {
            Text(text = "Bottom")

            var selectedCard by remember { mutableStateOf<Card?>(null) }
            Box(
                modifier = Modifier
                    .align(Alignment.Center)
                    .offset(0.dp, 0.4 * spread)
            ) {
                cards.mapIndexed { index, card ->
                    val middle by derivedStateOf { index - (cards.size / 2) }
                    val centre by derivedStateOf {
                        abs(index - cards.size / 2).toDouble().pow(2).toInt()
                    }

                    if (true) {
                        Box(
                            modifier = Modifier
                                .size(spread, 1.5 * spread)
                                .zIndex(index * 1f)
                                .offset(0.4 * spread * middle, 2.dp * centre)
                                .rotate(5f * middle)
                                .background(Color.Blue, shape = RoundedCornerShape(spread/10)),
                        )
                    } else {
                        CardView(
                            card = card,
                            modifier = Modifier
                                .size(spread, 1.5 * spread)
                                .zIndex(index * 1f)
                                .offset(0.4 * spread * middle, 2.dp * centre)
                                .rotate(5f * middle),
                            yOffset = if (card == selectedCard) -0.2 * spread else 0.dp,
                            onClick = {
                                if (selectedCard != card && allowed.contains(it)) selectedCard = it
                                else if (allowed.contains(it)) onPlay(index)
                            }
                        )
                    }
                }
            }
        }

        Box(
            modifier = Modifier
                .fillMaxSize(0.5f)
                .background(Color.Magenta)
                .align(Alignment.Center), contentAlignment = Alignment.Center
        ) {
            Text(text = "Center")

            // Left
            Box(
                modifier = Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex( 1f)
                    .offset(-0.7*spread, 0.2*spread)
                    .background(Color.Red)
            )

            // Top
            Box(
                modifier = Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex( 1f)
                    .offset(-0.1*spread, -0.6 * spread)
                    .background(Color.Yellow)
            )

            // Right
            Box(
                modifier = Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex( 1f)
                    .offset(0.7*spread, -0.2*spread)
                    .background(Color.Green)
            )

            // Bottom
            Box(
                modifier = Modifier
                    .size(spread, 1.5 * spread)
                    .zIndex( 1f)
                    .offset(0.1*spread, 0.6 * spread)
                    .background(Color.Blue)
            )
        }
    }
}

class NoClient : RikClient {
    override val events: Flow<List<RikGame.Event>>
        get() = flowOf()
    override val state: Flow<RikGame.State>
        get() = TODO("Not yet implemented")
    override val username: String
        get() = TODO("Not yet implemented")
    override val room: RoomID
        get() = TODO("Not yet implemented")
    override val playerId: PlayerId
        get() = TODO("Not yet implemented")

    override suspend fun start() {
        TODO("Not yet implemented")
    }

    override fun chat(msg: String) {
        TODO("Not yet implemented")
    }

    override fun begin() {
        TODO("Not yet implemented")
    }

    override fun bid(bid: RikType) {
        TODO("Not yet implemented")
    }

    override fun confirm(troef: Suit?, maat: Suit?, rikType: RikType) {
        TODO("Not yet implemented")
    }

    override fun play(cardIndex: Int) {
        TODO("Not yet implemented")
    }

    override fun stop() {
        TODO("Not yet implemented")
    }
}
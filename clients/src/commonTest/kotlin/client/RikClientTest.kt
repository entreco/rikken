package client

import RikClient
import RikGame
import RikGame.Companion.deserialize
import RoomID
import Settings
import io.ktor.server.application.install
import io.ktor.server.config.MapApplicationConfig
import io.ktor.server.routing.routing
import io.ktor.server.testing.ApplicationTestBuilder
import io.ktor.server.testing.testApplication
import io.ktor.server.websocket.webSocketRaw
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import io.ktor.client.plugins.websocket.WebSockets as ClientWebSockets
import io.ktor.server.websocket.WebSockets as ServerWebSockets

class RikClientTest {

    @Test
    @Ignore
    fun testClient() = testApplication {

        val room = RoomID.random()
        val username = "Remco"
        val client = givenRikClient(username, room)

        environment {
            config = MapApplicationConfig(
                listOf(
                    "ktor.deployment.host" to Settings.Test.host,
                    "ktor.deployment.port" to Settings.Test.port.toString()
                )
            )
        }

        givenServer { event ->
            when (event) {
                is RikGame.Event.Begin -> assertEquals(RikGame.Event.Begin(room), event)
                else -> assertTrue(true)
            }
        }

        client.start()
        assertEquals(client.events.first(), listOf(RikGame.Event.Chat("Hoi")))
        client.begin()
    }

    private fun ApplicationTestBuilder.givenRikClient(username: String, room: RoomID): RikClient =
        KtorClient(
            username = username,
            room = room,
            settings = Settings.Test,
            client = createClient { install(ClientWebSockets) }
        )

    private fun ApplicationTestBuilder.givenServer(assert: (RikGame) -> Unit) {
        application {
            //            createTestEnvironment { parentCoroutineContext = coroutineContext }
            install(ServerWebSockets)
            routing {
                webSocketRaw(path = "/server") {
                    // Mock server behaviour
                    send(Frame.Text("/hoi"))

                    for (frame in incoming) {
                        // assert responses from client were received
                        frame as Frame.Text? ?: continue
                        frame.readText().apply {
                            println("Received: $this")
                            assert(deserialize())
                        }
                    }
                }
            }
        }
    }
}
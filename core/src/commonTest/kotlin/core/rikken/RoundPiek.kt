package core.rikken

import kotlin.test.*

class RoundPiek {
    private lateinit var subject: Round

    @Test
    fun `should say NotFinished initially`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        assertTrue(subject.isNotFinished)
    }

    @Test
    fun `should have no battle initially`() {
        val settings = givenPiekSetting(piekers = listOf(1))
        subject = givenRound(settings, 0, 1, 2, 3)
        assertNull(subject.lastBattle())
    }

    @Test
    fun `should return player 0 as current initially`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        thenCurrentPlayerIs(0)
    }

    @Test
    fun `should return no allowedCards when hand is empty()`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        assertEquals(0, subject.allowedCardsFor(emptyList()).size)
    }

    @Test
    fun `should update current player when a card is played()`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))
        thenCurrentPlayerIs(1)
    }

    @Test
    fun `should call 'onAccepted' when a card is played()`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        val result = whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))
        assertTrue(result)
    }

    @Test
    fun `should NOT update current player when a card is played by a player that is not in turn`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(2, Card(Rank.TWO, Suit.HEARTS))
        thenCurrentPlayerIs(0)
    }

    @Test
    fun `should NOT update allowedCards when a card is played by a player that is not in turn`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(2, Card(Rank.TWO, Suit.HEARTS))

        // All Cards allowed, when no cards have been played
        thenAllowedCardsForHandIs(52, deck)
    }

    @Test
    fun `should NOT call 'onAccepted' when a card is played by a player that is not in turn`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        val result = whenPlaying(2, Card(Rank.TWO, Suit.HEARTS))
        assertFalse(result)
    }

    @Test
    fun `should update allowedCards when a card is played`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))

        // Only Hearts are allowed after playing H2 ;)
        thenAllowedCardsForHandIs(13, deck)
    }

    @Test
    fun `should allow last card to be played, when player cannot confes`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))

        // Only Mate Card (Heats of Ace) is allowed
        thenAllowedCardsForHandIs(1, arrayListOf(Card(Rank.TEN, Suit.DIAMONDS)))
    }

    @Test
    fun `should determine who won the Slag - normal case`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.FOUR, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))

        thenCurrentPlayerIs(3)
    }

    @Test
    fun `should determine who won the Slag - not confessing case`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.FOUR, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.DIAMONDS))

        thenCurrentPlayerIs(2)
    }

    @Test
    fun `should determine who won the Slag - aftroeven case`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)
        whenPlaying(0, Card(Rank.TWO, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.FOUR, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.DIAMONDS))

        thenCurrentPlayerIs(2)
    }

    @Test
    fun `should determine isFinished after playing 13 rounds`() {
        val settings = givenPiekSetting(piekers = listOf(1))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(12) {
            whenPlayingBattleWhichIsWonByPlayer1()
            assertTrue(subject.isNotFinished)
        }

        whenPlayingBattleWhichIsWonByPlayer1()
        assertFalse(subject.isNotFinished)
    }

    @Test
    fun `should determine isFinished after getting 2 slagen`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(1) {
            whenPlayingBattleWhichIsWonByPlayer1()
            assertTrue(subject.isNotFinished)
        }

        whenPlayingBattleWhichIsWonByPlayer1()
        assertFalse(subject.isNotFinished)
    }

    @Test
    fun `should determine isFinished after getting 2 slagen - with 2 piekers`() {
        val settings = givenPiekSetting(piekers = listOf(0, 2))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(4) {
            whenPlayingBattleWhichIsWonByPlayer1()
            assertTrue(subject.isNotFinished)
        }

        repeat(1) {
            whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
            whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
            whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
            whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
            assertTrue(subject.isNotFinished)
        }

        whenPlayingBattleWhichIsWonByPlayer3()
        assertFalse(subject.isNotFinished)
    }

    @Test
    fun `should determine isFinished after getting 2 slagen - with 1 piek and 1 misere`() {
        val settings = givenPiekSetting(piekers = listOf(0), miserders = listOf(2))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(4) {
            whenPlayingBattleWhichIsWonByPlayer1()
            assertTrue(subject.isNotFinished)
        }

        whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
        assertFalse(subject.isNotFinished)
    }

    @Test
    fun `should determine isFinished after getting 2 slagen - with 2 misereders`() {
        val settings = givenPiekSetting(miserders = listOf(0, 2))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(4) {
            whenPlayingBattleWhichIsWonByPlayer1()
            assertTrue(subject.isNotFinished)
        }

        whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
        assertFalse(subject.isNotFinished)
    }

    @Test
    fun `should allow all cards after a battle was finished`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)

        whenPlayingBattleWhichIsWonByPlayer1()

        // All Cards allowed, when 1 complete battle has been played
        thenAllowedCardsForHandIs(52, deck)
    }

    @Test
    fun `should play until the end if pieker has options left`() {
        val settings = givenPiekSetting(piekers = listOf(0))
        subject = givenRound(settings, 0, 1, 2, 3)

        repeat(1) {
            whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
            whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
            whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
            whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
            assertTrue(subject.isNotFinished)
        }

        repeat(11){
            whenPlayingBattleWhichIsWonByPlayer3()
            assertTrue(subject.isNotFinished)
        }

        assertTrue(subject.isNotFinished)
        whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
        assertTrue(subject.isNotFinished)
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
        assertTrue(subject.isNotFinished)
        whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
        assertTrue(subject.isNotFinished)
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        assertFalse(subject.isNotFinished)
    }

    private fun givenPiekSetting(
        piekers: List<Int> = emptyList(),
        miserders: List<Int> = emptyList(),
        type: RikType = RikType.Piek
    ) = Setting.NoRik(
        piekers.map { it },
        miserders.map { it },
        type
    )

    private fun givenRound(setting: Setting, vararg players: Int) = Round(
        setting,
        0,
        players.toList()
    )

    private fun whenPlaying(player: Int, card: Card) = subject.play(player, card)


    private fun whenPlayingBattleWhichIsWonByPlayer1() {
        whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
        whenPlaying(2, Card(Rank.FOUR, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
    }

    private fun whenPlayingBattleWhichIsWonByPlayer3() {
        whenPlaying(2, Card(Rank.ACE, Suit.HEARTS))
        whenPlaying(3, Card(Rank.FIVE, Suit.HEARTS))
        whenPlaying(0, Card(Rank.TEN, Suit.HEARTS))
        whenPlaying(1, Card(Rank.THREE, Suit.HEARTS))
    }

    private fun thenCurrentPlayerIs(expected: Int) {
        assertEquals(expected, subject.currentPlayer)
    }

    private fun thenAllowedCardsForHandIs(
        expected: Int,
        cards: List<Card>
    ) {
        assertEquals(expected, subject.allowedCardsFor(cards).size)
    }
}
package core.rikken

import kotlin.test.*

class RikTypeTest {

    @Test
    fun `all types should have unique Id to select them individually`() {
        assertEquals(RikTypeRules.all.groupBy { it.uniqueId }.size, RikTypeRules.all.distinct().size)
    }

    @Test
    fun `some types can be played simultaneously `() {
        assertNotEquals(RikTypeRules.all.groupBy { it.rank }.size, RikTypeRules.all.distinct().size)
    }

    @Test
    fun `should have 16 different ranks`() {
        assertEquals(16, RikTypeRules.all.groupBy { it.rank }.size)
    }

    @Test
    fun `should have 13 distinct rik type`() {
        assertEquals(19, RikTypeRules.all.distinct().size)
    }

    @Test
    fun `allowed and from should be symmetric`() {
        val allowed = RikTypeRules.allowed(emptyMap())
        val number5 = allowed[5]
        assertEquals(number5, RikTypeRules.from(number5.uniqueId))
    }

    @Test
    fun `when all bids are empty, 12 options are allowed`() {
        val allowed = given()
        assertEquals(12, allowed.size)
    }

    @Test
    fun `when no bids, Pas is allowed`() {
        val subject = given()
        subject.thenAllowed(RikType.Pas)
    }

    @Test
    fun `when no bids, Rik is allowed`() {
        val subject = given()
        subject.thenAllowed(RikType.Rik)
    }

    @Test
    fun `when no bids, Piek is allowed`() {
        val subject = given()
        subject.thenAllowed(RikType.Piek)
    }

    @Test
    fun `when no bids, Misere is allowed`() {
        val subject = given()
        subject.thenAllowed(RikType.Misere)
    }

    @Test
    fun `when no bids, RikBeter is NOT allowed`() {
        val subject = given()
        subject.thenNotAllowed(RikType.RikBeter)
    }

    @Test
    fun `when Rik, Beter is allowed`() {
        val subject = given(RikType.Rik)
        subject.thenAllowed(RikType.RikBeter)
    }

    @Test
    fun `when Rik, Pas is allowed`() {
        val subject = given(RikType.Rik)
        subject.thenAllowed(RikType.Pas)
    }

    @Test
    fun `when Rik, Piek is allowed`() {
        val subject = given(RikType.Rik)
        subject.thenAllowed(RikType.Piek)
    }

    @Test
    fun `when Pas,Pas,Pas then Mieen is an option`() {
        val subject = given(RikType.Pas, RikType.Pas, RikType.Pas)
        subject.thenAllowed(RikType.Mieen)
    }

    @Test
    fun `when Pas,Pas,Pas then Rik is an option`() {
        val subject = given(RikType.Pas, RikType.Pas, RikType.Pas)
        subject.thenAllowed(RikType.Rik)
    }

    @Test
    fun `when Pas,Pas,Pas then Pas is NOT an option`() {
        val subject = given(RikType.Pas, RikType.Pas, RikType.Pas)
        subject.thenNotAllowed(RikType.Pas)
    }

    @Test
    fun `when Pas,Pas,Pas then Misere is an option`() {
        val subject = given(RikType.Pas, RikType.Pas, RikType.Pas)
        subject.thenAllowed(RikType.Misere)
    }

    @Test
    fun `ranks should be 0`() {
        assertEquals(0, RikType.Pas.rank)
    }

    @Test
    fun `ranks should be 1`() {
        assertEquals(1, RikType.Rik.rank)
    }

    @Test
    fun `ranks should be 2`() {
        assertEquals(2, RikType.RikBeter.rank)
    }

    @Test
    fun `ranks should be 3`() {
        assertEquals(3, RikType.Piek.rank)
        assertEquals(3, RikType.Misere.rank)
    }

    @Test
    fun `ranks should be 4`() {
        assertEquals(4, RikType.Solo10.rank)
    }

    @Test
    fun `ranks should be 5`() {
        assertEquals(5, RikType.Solo10Beter.rank)
    }

    @Test
    fun `ranks should be 6`() {
        assertEquals(6, RikType.PiekOpen.rank)
        assertEquals(6, RikType.MisereOpen.rank)
    }

    @Test
    fun `ranks should be 7`() {
        assertEquals(7, RikType.Solo11.rank)
    }

    @Test
    fun `ranks should be 8`() {
        assertEquals(8, RikType.Solo11Beter.rank)
    }

    @Test
    fun `ranks should be 9`() {
        assertEquals(9, RikType.PiekPraatje.rank)
        assertEquals(9, RikType.MiserePraatje.rank)
    }

    @Test
    fun `ranks should be 10`() {
        assertEquals(10, RikType.Solo12.rank)
    }

    @Test
    fun `ranks should be 11`() {
        assertEquals(11, RikType.Solo12Beter.rank)
    }

    @Test
    fun `ranks should be 12`() {
        assertEquals(12, RikType.Solo13.rank)
    }

    @Test
    fun `ranks should be 13`() {
        assertEquals(13, RikType.Solo13Beter.rank)
    }

    private fun given(vararg bids: RikType) =
        RikTypeRules.allowed(bids.mapIndexed { index, type -> index to type }.toMap())

    private fun List<RikType>.thenAllowed(vararg expected: RikType) {
        expected.forEach {
            assertTrue(contains(it))
        }
    }

    private fun List<RikType>.thenNotAllowed(vararg expected: RikType) {
        expected.forEach {
            assertFalse(contains(it))
        }
    }
}
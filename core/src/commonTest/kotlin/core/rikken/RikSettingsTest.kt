package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals

class RikSettingsTest {

    @Test
    fun `should confirm Mieëen when everyone passes`() {
        val subject = givenSubject(RikType.Pas, RikType.Pas, RikType.Pas, RikType.Mieen)
        val actions = subject.generateActions()
        assertEquals(actions, Actions.ConfirmMieen)
    }

    private fun givenSubject(vararg types: RikType) = RikSettings(
        bids = types.mapIndexed { index: Int, rikType: RikType -> index to rikType }.toMap(),
        hands = emptyList()
    )
}
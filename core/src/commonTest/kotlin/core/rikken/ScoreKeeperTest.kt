package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ScoreKeeperTest {

    private val subject = ScoreKeeper()

    @Test
    fun `should have score of zero for all initially`() {
        givenPlayers("p1", "p2", "p3", "p4")
        subject.scores().forEach {
            assertEquals(0, it.current)
        }
    }

    @Test
    fun `should have delta of zero for all initially`() {
        givenPlayers("p1", "p2", "p3", "p4")
        subject.scores().forEach {
            assertEquals(0, it.delta)
        }
    }

    @Test
    fun `should have total of zero for all initially`() {
        givenPlayers("p1", "p2", "p3", "p4")
        subject.scores().forEach {
            assertEquals(0, it.total)
        }
    }

    @Test
    fun `should throw when updating scores with a non-finished round`() {
        givenPlayers("p1", "p2")
        assertFailsWith<IllegalArgumentException> {
            subject += givenRound(
                listOf(1), Setting.Rik(0, Suit.HEARTS, Card(Rank.ACE, Suit.CLUBS), RikType.Rik, 8, null)
            )
        }
    }

    @Test
    fun `should updating scores when applying a finished round or Rikken`() {
        val settings = Setting.Rik(0, Suit.HEARTS, Card(Rank.ACE, Suit.CLUBS), RikType.Rik, 8, null)
        val players = givenPlayers("p1", "p2", "Henkie", "Joep")
        subject += givenCompletedRound(players, settings)

        assertEquals(-4, subject.scores()[0].total)
        assertEquals(-4, subject.scores()[1].total)
        assertEquals(4, subject.scores()[2].total)
        assertEquals(4, subject.scores()[3].total)
    }

    @Test
    fun `should updating scores when applying a finished round or Solo Rik`() {
        val settings = Setting.Solo(0, Suit.DIAMONDS, RikType.Solo10, 10)
        val players = givenPlayers("p1", "p2", "Henkie", "Joep")
        subject += givenCompletedRound(players, settings)

        assertEquals(-144, subject.scores()[0].total)
        assertEquals(48, subject.scores()[1].total)
        assertEquals(48, subject.scores()[2].total)
        assertEquals(48, subject.scores()[3].total)
    }

    @Test
    fun `should updating scores when applying a finished round of Pieken`() {
        val settings = Setting.NoRik(listOf(0,1), listOf(2,3), RikType.Piek)
        val players = givenPlayers("p1", "p2", "Henkie", "Joep")
        subject += givenCompletedRound(players, settings)

        assertEquals(0, subject.scores()[0].total)
        assertEquals(0, subject.scores()[1].total)
        assertEquals(0, subject.scores()[2].total)
        assertEquals(0, subject.scores()[3].total)
    }

    @Test
    fun `should updating scores when applying a finished round of Misère`() {
        val settings = Setting.NoRik(listOf(0,1), listOf(2,3), RikType.Misere)
        val players = givenPlayers("p1", "p2", "Henkie", "Joep")
        subject += givenCompletedRound(players, settings)

        assertEquals(0, subject.scores()[0].total)
        assertEquals(0, subject.scores()[1].total)
        assertEquals(0, subject.scores()[2].total)
        assertEquals(0, subject.scores()[3].total)
    }

    private fun givenRound(players: List<Int>, setting: Setting) = Round(
        setting = setting,
        startingPlayer = 0,
        players = players
    )

    private fun givenCompletedRound(players: List<Int>, setting: Setting): Round {
        val round = givenRound(players,setting)
        val deck = Suit.entries.flatMap { s -> Rank.entries.map { r -> Card(r, s) } }.toMutableList()

        while(deck.isNotEmpty()) {
            round.play(round.currentPlayer, deck.removeAt(0))
        }

        return round
    }

    private fun givenPlayers(
        vararg players: String
    ) : List<Int> = players.mapIndexed { index, s ->
        subject.onPlayerJoined(index)
        index
    }

    private fun ScoreKeeper.scores(): List<Score> = scores.values.toList()
}
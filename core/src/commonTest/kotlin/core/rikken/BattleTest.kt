package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue


class BattleTest {

    @Test
    fun `should set No Winner when no cards played in 'Slag'`() {
        val rikSetting = givenRikSetting(Suit.HEARTS)
        val subject = givenSlag()
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIsNull()
    }

    @Test
    fun `should set No Winner when single card played in 'Slag'`() {
        val rikSetting = givenRikSetting(Suit.HEARTS)
        val subject = givenSlag(Card(Rank.ACE, Suit.HEARTS))
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIsNull()
    }

    @Test
    fun `should set No Winner when two cards played in 'Slag'`() {
        val rikSetting = givenRikSetting(Suit.HEARTS)
        val subject = givenSlag(Card(Rank.EIGHT, Suit.CLUBS), Card(Rank.ACE, Suit.HEARTS))
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIsNull()
    }

    @Test
    fun `should set No Winner when three cards played in 'Slag'`() {
        val rikSetting = givenRikSetting(Suit.HEARTS)
        val subject = givenSlag(
            Card(Rank.JACK, Suit.DIAMONDS),
            Card(Rank.EIGHT, Suit.CLUBS),
            Card(Rank.ACE, Suit.HEARTS)
        )
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIsNull()
    }

    @Test
    fun `should determine Winner when all cards played in 'Slag' with troef=Hearts`() {
        val rikSetting = givenRikSetting(troef = Suit.HEARTS)
        val subject = givenSlag(
            Card(Rank.THREE, Suit.SPADES),
            Card(Rank.ACE, Suit.SPADES),
            Card(Rank.TWO, Suit.HEARTS),
            Card(Rank.ACE, Suit.DIAMONDS)
        )
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIs(2)
    }

    @Test
    fun `should determine Winner when all cards played in 'Slag' without troef`() {
        val rikSetting = givenRikSetting(troef = Suit.HEARTS)
        val subject = givenSlag(
            Card(Rank.THREE, Suit.SPADES),
            Card(Rank.ACE, Suit.SPADES),
            Card(Rank.TWO, Suit.CLUBS),
            Card(Rank.ACE, Suit.DIAMONDS)
        )
        subject.whenCheckingWinner(rikSetting)
        subject.thenWinnerIs(1)
    }

    @Test
    fun `should return true when battle contains card`() {
        val card1 = Card(Rank.THREE, Suit.SPADES)
        val card2 = Card(Rank.ACE, Suit.SPADES)
        val card3 = Card(Rank.TWO, Suit.CLUBS)
        val card4 = Card(Rank.ACE, Suit.DIAMONDS)

        val subject = givenSlag(card1, card2, card3, card4)

        assertTrue(subject.containsCard(card1))
        assertTrue(subject.containsCard(card2))
        assertTrue(subject.containsCard(card3))
        assertTrue(subject.containsCard(card4))
    }

    @Test
    fun `should return false when battle does NOT contain card`() {
        val card = Card(Rank.THREE, Suit.SPADES)
        val card2 = Card(Rank.ACE, Suit.SPADES)
        val card3 = Card(Rank.TWO, Suit.CLUBS)
        val card4 = Card(Rank.ACE, Suit.DIAMONDS)

        val subject = givenSlag(card2, card2, card3, card4)

        assertFalse(subject.containsCard(card))
    }

    private fun givenRikSetting(troef: Suit) =
        Setting.Rik(0, troef, Card(Rank.ACE, Suit.SPADES), RikType.Rik, 8)

    private fun givenSlag(vararg cards: Card): Battle {
        return Battle().apply {
            cards.forEachIndexed { index, card ->
                add(index to card)
            }
        }
    }

    private fun Battle.whenCheckingWinner(setting: Setting.Rik) {
        determineWinner(setting)
    }

    private fun Battle.thenWinnerIsNull() {
        assertNull(winner())
    }

    private fun Battle.thenWinnerIs(expected: Int) {
        assertEquals(playerAt(expected), winner())
    }

}
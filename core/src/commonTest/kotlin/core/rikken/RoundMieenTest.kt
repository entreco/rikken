package core.rikken

import kotlin.test.Test
import kotlin.test.assertTrue

class RoundMieenTest {

    private val fullHand = deck

    @Test
    fun `should print allowed Cards for Mieen - round 1`() {
        val settings = givenSettingsForMieen()
        val subject = givenRound(settings, 0,1,2,3)
        val allowed = subject.allowedCardsFor(fullHand)

        assertTrue(allowed.none { it.suit == Suit.SPADES })
        subject.whenPlaying(0, allowed.first())
    }

    @Test
    fun `should print allowed Cards for Mieen - round 6`() {
        val settings = givenSettingsForMieen()
        val subject = givenRound(settings, 0,1,2,3)

        repeat(5) {
            subject.whenPlaying(0, subject.allowedCardsFor(fullHand).first())
            subject.whenPlaying(1, subject.allowedCardsFor(fullHand).first())
            subject.whenPlaying(2, subject.allowedCardsFor(fullHand).first())
            subject.whenPlaying(3, subject.allowedCardsFor(fullHand).first())
        }

        val allowed = subject.allowedCardsFor(fullHand)
        assertTrue(allowed.count { it.suit == Suit.SPADES } == 13)
    }

    private fun Round.whenPlaying(player: Int, card: Card) {
        play(player, card)
    }

    private fun givenSettingsForMieen() = Setting.Mieeen

    private fun givenRound(setting: Setting, vararg players: Int) = Round(
        setting,
        0,
        players.toList()
    )

}
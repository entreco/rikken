package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals

class BidManagerTest {

    private lateinit var subject: BidManager

    @Test
    fun `should set all bids to None initially`() {
        subject = givenManager("p1", "p2")
        thenBids().all { it.value == RikType.None }
    }

    @Test
    fun `should set areSettled to false initially`() {
        subject = givenManager("p1", "p2")
        thenAreSettledIs(false)
    }

    @Test
    fun `should set areSettled to false after 1st player Riks`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        thenAreSettledIs(false)
    }

    @Test
    fun `should add Rik when 1st player Riks`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        thenBidIs(0 to RikType.Rik)
        thenBidIs(1 to RikType.None)
    }

    @Test
    fun `should add Rik when 1st player Riks - different syntax`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        thenBidsAre(RikType.Rik, RikType.None)
    }

    @Test
    fun `should set areSettled true when 1st player Riks and 2nd player Pas`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        thenAreSettledIs(true)
    }

    @Test
    fun `should set Bids when Rikking & Passing`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        thenBidsAre(RikType.Rik, RikType.Pas)
    }

    @Test
    fun `should set Bids when Rikking, Beter, Piek, Misere `() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.RikBeter)
        whenBidding(0, RikType.Piek)
        whenBidding(1, RikType.Misere)
        thenBidsAre(RikType.Piek, RikType.Misere)
    }

    @Test
    fun `should set Bids when going all the way up to Solo13`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.RikBeter)
        thenBidsAre(RikType.None, RikType.RikBeter)
        whenBidding(0, RikType.Piek)
        whenBidding(1, RikType.Solo10)
        thenBidsAre(RikType.None, RikType.Solo10)
        whenBidding(0, RikType.Solo10Beter)
        whenBidding(1, RikType.PiekOpen)
        thenBidsAre(RikType.None, RikType.PiekOpen)
        whenBidding(0, RikType.Solo11)
        whenBidding(1, RikType.Solo11Beter)
        thenBidsAre(RikType.None, RikType.Solo11Beter)
        whenBidding(0, RikType.PiekPraatje)
        whenBidding(1, RikType.Solo12)
        thenBidsAre(RikType.None, RikType.Solo12)
        whenBidding(0, RikType.Solo12Beter)
        whenBidding(1, RikType.Solo13)
        thenBidsAre(RikType.None, RikType.Solo13)
        whenBidding(0, RikType.Solo13Beter)
        whenBidding(1, RikType.Pas)
        thenBidsAre(RikType.Solo13Beter, RikType.Pas)
    }

    @Test
    fun `should set Bids with Mieeen`() {
        subject = givenManager("p1", "p2")
        whenBidding(0, RikType.Pas)
        whenBidding(1, RikType.Pas)
        thenBidsAre(RikType.Pas, RikType.Mieen)
    }

    @Test
    fun `should prevent people from going before their turn`() {
        subject = givenManager( "p1", "p2")
        whenBidding(1, RikType.Pas)
        thenBidsAre(RikType.None, RikType.None)
    }

    @Test
    fun `should prevent people from going before their turn - when starting player is 1`() {
        subject = givenManager(1,  "p1", "p2")
        whenBidding(0, RikType.Pas)
        thenBidsAre(RikType.None, RikType.None)
    }

    @Test
    fun `should allow starting Player to bid first when starting player is 1`() {
        subject = givenManager(1,  "p1", "p2")
        whenBidding(1, RikType.Piek)
        thenBidsAre(RikType.None, RikType.Piek)
    }

    @Test
    fun `should allow last Player to bid first when starting player is 3`() {
        subject = givenManager(3,  "p1", "p2","p3", "i wanna start")
        whenBidding(3, RikType.RikBeter)
        thenBidsAre(RikType.None, RikType.None, RikType.None, RikType.RikBeter)
        thenAreSettledIs(false)
    }

    @Test
    fun `should keep track of current player when over bidding`() {
        subject = givenManager(0, "p1", "p2","p3", "i wanna start")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Piek)
        thenCurrentPlayerIs(2)
    }

    @Test
    fun `should keep track of current player when over bidding multiple times`() {
        subject = givenManager(1, "p1", "p2","p3", "p4")
        thenCurrentPlayerIs(1)
        whenBidding(1, RikType.Rik)
        thenCurrentPlayerIs(2)
        whenBidding(2, RikType.Piek)
        thenCurrentPlayerIs(3)
        whenBidding(3, RikType.Solo10)
        thenCurrentPlayerIs(0)
        whenBidding(0, RikType.Solo10Beter)
        thenCurrentPlayerIs(1)
    }

    @Test
    fun `should keep track of current player when playing another round`() {
        subject = givenManager(0, "p1", "p2","p3", "p4")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        whenBidding(2, RikType.Pas)
        whenBidding(3, RikType.Pas)
        thenCurrentPlayerIs(1)
    }

    @Test
    fun `should keep track of current player when playing multiple rounds`() {
        subject = givenManager(0, "p1", "p2","p3", "p4")
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        whenBidding(2, RikType.Pas)
        whenBidding(3, RikType.Pas)
        thenCurrentPlayerIs(1)
        whenBidding(1, RikType.Pas)
        whenBidding(2, RikType.Pas)
        whenBidding(3, RikType.Pas)
        whenBidding(0, RikType.Rik)
        thenCurrentPlayerIs(2)
        whenBidding(2, RikType.Pas)
        whenBidding(3, RikType.Pas)
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        thenCurrentPlayerIs(3)
        whenBidding(3, RikType.Pas)
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        whenBidding(2, RikType.Pas)
        thenCurrentPlayerIs(0)
        whenBidding(0, RikType.Rik)
        whenBidding(1, RikType.Pas)
        whenBidding(2, RikType.Pas)
        whenBidding(3, RikType.Pas)
        thenCurrentPlayerIs(1)
    }

    private fun givenManager(vararg players: String) = BidManager(players.size, 0)

    private fun givenManager(startPlayer: Int, vararg players: String) = BidManager(players.size, startPlayer)

    private fun whenBidding(player: Int, type: RikType) {
        subject += Pair(player, type)
    }

    private fun thenAreSettledIs(expected: Boolean) {
        assertEquals(expected, subject.areSettled)
    }

    private fun thenBids() = subject.bids()

    private fun thenBidIs(expected: Pair<Int, RikType>) {
        assertEquals(expected.second, subject.bids()[expected.first])
    }

    private fun thenBidsAre(vararg expected: RikType) {
        subject.bids().toList().forEachIndexed { index, pair ->
            assertEquals(expected[index], pair.second)
        }
    }

    private fun thenCurrentPlayerIs(expected: Int?) {
        assertEquals(expected, subject.currentPlayer)
    }
}
package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals

class ScoreTest {

    @Test
    fun `should have total score of zero initially`() {
        assertEquals(0, Score().total)
    }

    @Test
    fun `should have delta score of zero initially`() {
        assertEquals(0, Score().delta)
    }

    @Test
    fun `should have prev score of zero initially`() {
        assertEquals(0, Score().prev)
    }

    @Test
    fun `should add amount to score`() {
        assertEquals(20, (Score() + 20).total)
    }

    @Test
    fun `should add amount to score - delta `() {
        assertEquals(20, (Score() + 20).delta)
    }

    @Test
    fun `should add amount to score - prev`() {
        assertEquals(0, (Score() + 20).prev)
    }

    @Test
    fun `should add amount to score and subtract`() {
        assertEquals(21, (Score() + 20 + -4 + 5).total)
    }

    @Test
    fun `should track history`() {
        assertEquals(3, (Score() + 20 + -4 + 5).history.size)
    }

    @Test
    fun `should track delta`() {
        assertEquals(5, (Score() + 20 + -4 + 5).delta)
    }

    @Test
    fun `should track previous`() {
        assertEquals(16, (Score() + 20 + -4 + 5).prev)
    }

    @Test
    fun `should track current`() {
        assertEquals(-2, (Score() + 20 + -4 + -18).current)
    }

    @Test
    fun `should track current staring with Nullable Score`() {
        val score : Score? = null
        assertEquals(-2, (score + 20 + -4 + -18).current)
    }

    @Test
    fun `should track prev staring with Nullable Score`() {
        val score : Score? = null
        assertEquals(-4, (score + -4 + -18).prev)
    }

    @Test
    fun `should track delta staring with Nullable Score`() {
        val score : Score? = null
        assertEquals(-18, (score + -18).delta)
    }
}
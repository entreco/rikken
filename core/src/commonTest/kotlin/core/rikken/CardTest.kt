package core.rikken

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class CardTest {

    @Test
    fun `should display Suit and Rank card (H2)`() {
        assertEquals("♥2", Card(Rank.TWO, Suit.HEARTS).toString())
    }

    @Test
    fun `should display Suit and Rank card (AS)`() {
        assertEquals("♠A", Card(Rank.ACE, Suit.SPADES).toString())
    }

    @Test
    fun `should display Suit and Rank card (C10)`() {
        assertEquals("♣10", Card(Rank.TEN, Suit.CLUBS).toString())
    }

    @Test
    fun `should display Suit and Rank card (DK)`() {
        assertEquals("♦K", Card(Rank.KING, Suit.DIAMONDS).toString())
    }

    @Test
    fun `should display Suit and Rank card (SQ)`() {
        assertEquals("♠Q", Card(Rank.QUEEN, Suit.SPADES).toString())
    }

    @Test
    fun testCardSorting() {
        val cards = listOf(Card(Rank.SIX, Suit.HEARTS), Card(Rank.EIGHT, Suit.CLUBS), Card(Rank.TWO, Suit.CLUBS), Card(Rank.TWO, Suit.HEARTS))
        val result = cards.sorted()
        assertEquals(Card(Rank.TWO, Suit.CLUBS), result[0])
        assertEquals(Card(Rank.EIGHT, Suit.CLUBS), result[1])
        assertEquals(Card(Rank.TWO, Suit.HEARTS), result[2])
        assertEquals(Card(Rank.SIX, Suit.HEARTS), result[3])
    }
}
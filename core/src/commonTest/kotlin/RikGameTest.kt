import RikGame.Companion.deserialize
import core.rikken.RikSettings
import core.rikken.RikType
import core.rikken.ScoreKeeper
import core.rikken.Setting
import kotlinx.coroutines.test.runTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class RikGameTest {

    @Test
    fun testRawString() {
        val json = RikGame.State.Welcome("welcome").serialize()
        val result = json.deserialize()
        assertTrue(result is RikGame.State.Welcome)
    }

    @Test
    fun testJsonBidding() {
        val json = RikGame.State.Bidding(emptyList(), emptyList(), 0, 0, emptyMap()).serialize()
        val result = json.deserialize()
        assertTrue(result is RikGame.State.Bidding)
    }

    @Test
    fun testRawStringBidding() {
        val json = """
            /bidding {"players":[{"playerIndex":0,"name":"Dikie"},{"playerIndex":1,"name":"Bot 1","bot":true},{"playerIndex":2,"name":"Bot 2","bot":true},{"playerIndex":3,"name":"Bot 3","bot":true}],"hands":[[{"rank":"FIVE","suit":"CLUBS"},{"rank":"SIX","suit":"CLUBS"},{"rank":"QUEEN","suit":"CLUBS"},{"rank":"ACE","suit":"CLUBS"},{"rank":"FIVE","suit":"DIAMONDS"},{"rank":"SIX","suit":"DIAMONDS"},{"rank":"EIGHT","suit":"DIAMONDS"},{"rank":"FOUR","suit":"SPADES"},{"rank":"FIVE","suit":"SPADES"},{"rank":"KING","suit":"SPADES"},{"rank":"ACE","suit":"SPADES"},{"rank":"THREE","suit":"HEARTS"},{"rank":"SIX","suit":"HEARTS"}],[{"rank":"SEVEN","suit":"CLUBS"},{"rank":"TEN","suit":"CLUBS"},{"rank":"JACK","suit":"CLUBS"},{"rank":"THREE","suit":"DIAMONDS"},{"rank":"TEN","suit":"DIAMONDS"},{"rank":"JACK","suit":"DIAMONDS"},{"rank":"SEVEN","suit":"SPADES"},{"rank":"TEN","suit":"SPADES"},{"rank":"TWO","suit":"HEARTS"},{"rank":"EIGHT","suit":"HEARTS"},{"rank":"TEN","suit":"HEARTS"},{"rank":"JACK","suit":"HEARTS"},{"rank":"QUEEN","suit":"HEARTS"}],[{"rank":"THREE","suit":"CLUBS"},{"rank":"FOUR","suit":"CLUBS"},{"rank":"KING","suit":"CLUBS"},{"rank":"TWO","suit":"DIAMONDS"},{"rank":"FOUR","suit":"DIAMONDS"},{"rank":"NINE","suit":"DIAMONDS"},{"rank":"QUEEN","suit":"DIAMONDS"},{"rank":"ACE","suit":"DIAMONDS"},{"rank":"THREE","suit":"SPADES"},{"rank":"JACK","suit":"SPADES"},{"rank":"QUEEN","suit":"SPADES"},{"rank":"FIVE","suit":"HEARTS"},{"rank":"SEVEN","suit":"HEARTS"}],[{"rank":"TWO","suit":"CLUBS"},{"rank":"EIGHT","suit":"CLUBS"},{"rank":"NINE","suit":"CLUBS"},{"rank":"SEVEN","suit":"DIAMONDS"},{"rank":"KING","suit":"DIAMONDS"},{"rank":"TWO","suit":"SPADES"},{"rank":"SIX","suit":"SPADES"},{"rank":"EIGHT","suit":"SPADES"},{"rank":"NINE","suit":"SPADES"},{"rank":"FOUR","suit":"HEARTS"},{"rank":"NINE","suit":"HEARTS"},{"rank":"KING","suit":"HEARTS"},{"rank":"ACE","suit":"HEARTS"}]],"biddingPlayer":0,"bids":{"0":{"type":"core.rikken.RikType.None"},"1":{"type":"core.rikken.RikType.None"},"2":{"type":"core.rikken.RikType.None"},"3":{"type":"core.rikken.RikType.None"}}}
        """.trimIndent()
        val result = json.deserialize()
        assertTrue(result is RikGame.State.Bidding)
    }

    @Test
    fun testJsonSelecting() {
        val json = RikGame.State.Selecting(emptyList(), emptyList(), 0, RikSettings(emptyMap(), emptyList())).serialize()
        val result = json.deserialize()
        assertTrue(result is RikGame.State.Selecting)
    }

    @Test
    fun testJsonPlaying() {
        val json = RikGame.State.Playing(emptyList(), emptyList(), 0, 12, Setting.Mieeen, emptyList()).serialize()
        val result = json.deserialize()
        assertTrue(result is RikGame.State.Playing)
    }

    @Test
    fun playerTest() = runTest {
        val subject = Rikker(1, "Remco")
        assertEquals(1, subject.playerIndex)
        assertEquals("Remco", subject.name)
    }

    @Test
    fun roomIdTest() {
        repeat(1000) {
            assertNotEquals(RoomID.random(), RoomID.random())
        }
    }
}
import kotlinx.serialization.Serializable

@Serializable
data class Rikker(val playerIndex: Int, val name: String, val bot: Boolean = false) {
    override fun toString() = "$name($playerIndex)${if (bot) "*" else ""}"
    fun named(): String = if(bot) name + "\uD83E\uDD16"  else name
}
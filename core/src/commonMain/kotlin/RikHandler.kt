interface RikHandler {
    val username: String
    suspend fun chat(chat: RikGame.Event.Chat)
    suspend fun whisper(state: RikGame.State)
}
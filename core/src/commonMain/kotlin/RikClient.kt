import core.rikken.RikType
import core.rikken.Suit
import kotlinx.coroutines.flow.Flow

interface RikClient {
    val events: Flow<List<RikGame.Event>>
    val state: Flow<RikGame.State>
    val username: String
    val room: RoomID
    val playerId: PlayerId

    suspend fun start()

    fun chat(msg: String)
    fun begin()
    fun bid(bid: RikType)
    fun confirm(troef: Suit?, maat: Suit?, rikType: RikType)
    fun play(cardIndex: Int)
    fun stop()
}
interface RikServer {
    suspend fun chatAll(message: String)
    suspend fun join(sessionHandler: RikHandler, roomId: RoomID)
    suspend fun leave(sessionHandler: RikHandler)
}
package core.rikken

data class ScoreKeeper(
    val scores: MutableMap<Int, Score> = mutableMapOf()
) {

    fun onPlayerJoined(player: Int) {
        scores[player] = Score(0, 0, emptyList())
    }

    fun onPlayerLeft(player: Int?) {
        if (scores.containsKey(player)) scores.remove(player)
    }

    operator fun plusAssign(round: Round) {
        require(!round.isNotFinished) { "Only update scores after a complete round" }
        when (round.setting) {
            is Setting.Rik -> settleRik(round, round.setting)
            is Setting.Solo -> settleSolo(round, round.setting)
            is Setting.NoRik -> settleNoRik(round, round.setting)
            is Setting.Mieeen -> settleMieen(round, round.setting)
            is Setting.Invalid -> print("Setting Invalid")
        }
    }

    private fun settleRik(round: Round, setting: Setting.Rik) {
        val rikker = setting.player
        val maat = setting.maat

        val battles = round.battles().count { it.winner() == rikker || it.winner() == maat }
        val amount = when (val diff = battles - setting.goal) {
            0 -> setting.rikType.reward
            else -> diff * setting.rikType.reward
        }
        val rikkers = listOfNotNull(rikker, maat)
        rikkers.forEach {
            scores[it] = scores[it] + amount
        }

        val others = round.players - rikkers.toSet()
        others.forEach {
            scores[it] = scores[it] + (-1 * amount)
        }
    }


    private fun settleSolo(round: Round, setting: Setting.Solo) {
        val soloer = setting.player

        val battles = round.battles().count { it.winner() == soloer }
        val diff = battles - setting.goal
        val amount = when {
            diff == 0 -> setting.rikType.reward
            else -> diff * setting.rikType.reward
        }

        val payOuts = mutableMapOf<Int, Int>()
        val soloers = listOf(soloer)
        val others = round.players - soloers.toSet()
        others.forEach { player ->
            payOuts[soloer] = payOuts.getOrPut(soloer) { 0 }.plus(amount)
            payOuts[player] = payOuts.getOrPut(player) { 0 }.minus(amount)
        }

        round.players.forEach {
            val payout = payOuts[it]
            scores[it] = scores[it] + payout!!
        }
    }

    private fun settleNoRik(round: Round, setting: Setting.NoRik) {
        val payOuts = mutableMapOf<Int, Int>()
        val piekers = setting.piekers
        piekers.forEach { piek ->
            val count = round.battles().count { it.winner() == piek }
            val amount = (if (count == 1) 1 else -1) * setting.rikType.reward
            val others = round.players - piek
            others.forEach {
                payOuts[piek] = payOuts.getOrPut(piek) { 0 }.plus(amount)
                payOuts[it] = payOuts.getOrPut(it) { 0 }.minus(amount)
            }
        }

        val miserders = setting.miserders
        miserders.forEach { misere ->
            val count = round.battles().count { it.winner() == misere }
            val amount = (if (count == 0) 1 else -1) * setting.rikType.reward
            val others = round.players - misere
            others.forEach {
                payOuts[misere] = payOuts.getOrPut(misere) { 0 }.plus(amount)
                payOuts[it] = payOuts.getOrPut(it) { 0 }.minus(amount)
            }
        }

        round.players.forEach {
            val payout = payOuts[it]
            scores[it] = scores[it] + payout!!
        }
    }

    private fun settleMieen(round: Round, setting: Setting.Mieeen) {
        val payOuts = mutableMapOf<Int, Int>()

        val queen =
            round.battles().first { it.containsCard(Card(Rank.QUEEN, Suit.SPADES)) }.winner()!!
        var others = round.players - queen
        others.forEach {
            payOuts[queen] = payOuts.getOrPut(queen) { 0 }.minus(1 * RikType.Mieen.reward)
            payOuts[it] = payOuts.getOrPut(it) { 0 }.plus(1 * RikType.Mieen.reward)
        }

        val last = round.battles().last().winner()!!
        others = round.players - last
        others.forEach {
            payOuts[last] = payOuts.getOrPut(last) { 0 }.minus(1 * RikType.Mieen.reward)
            payOuts[it] = payOuts.getOrPut(it) { 0 }.plus(1 * RikType.Mieen.reward)
        }

        round.players.forEach {
            val payout = payOuts[it]
            scores[it] = scores[it] + payout!!
        }
    }
}

package core.rikken

import kotlinx.serialization.Serializable

@Serializable
data class RoundResult(val amount: Int)
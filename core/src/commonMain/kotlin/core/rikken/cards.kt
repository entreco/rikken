package core.rikken

import kotlinx.serialization.Serializable

val deck = Suit.entries.flatMap { s -> Rank.entries.map { r -> Card(r, s) } }

enum class Suit {
    CLUBS,
    DIAMONDS,
    SPADES,
    HEARTS,
    ;

    override fun toString() = when (this) {
        HEARTS -> "♥"
        CLUBS -> "♣"
        DIAMONDS -> "♦"
        SPADES -> "♠"
    }
}

enum class Rank {
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;

    override fun toString() = when (this) {
        TWO -> "2"
        THREE -> "3"
        FOUR -> "4"
        FIVE -> "5"
        SIX -> "6"
        SEVEN -> "7"
        EIGHT -> "8"
        NINE -> "9"
        TEN -> "10"
        JACK -> "J"
        QUEEN -> "Q"
        KING -> "K"
        ACE -> "A"
    }
}

@Serializable
data class Card(val rank: Rank, val suit: Suit) {
    override fun toString() = "$suit$rank"
}

fun List<Card>.sorted() = sortedBy { it.rank }.sortedBy { it.suit }
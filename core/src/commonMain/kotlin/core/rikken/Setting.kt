package core.rikken

import kotlinx.serialization.Serializable

@Serializable
sealed class Setting {

    fun players(): List<Int> = when (this) {
        is Rik -> listOf(player)
        is NoRik -> piekers + miserders
        else -> emptyList()
    }

    open fun describe(): String = ""

    open fun played(player: Int, card: Card) {}

    @Serializable
    data class Rik(
        val player: Int,
        val troef: Suit,
        val mate: Card,
        val rikType: RikType,
        val goal: Int = 8,
        var maat: Int? = null
    ) : Setting() {

        override fun played(player: Int, card: Card) {
            if (mate == card) {
                maat = player
            }
        }

        override fun describe(): String = "Rik (${goal})"
    }

    @Serializable
    data class Solo(
        val player: Int,
        val troef: Suit,
        val rikType: RikType,
        val goal: Int
    ) : Setting() {
        override fun describe(): String = "${rikType.desc}(${goal})"
    }

    @Serializable
    data class NoRik(
        val piekers: List<Int>,
        val miserders: List<Int>,
        val rikType: RikType
    ) : Setting(){
        override fun describe(): String = rikType.desc
    }

    @Serializable
    data object Mieeen : Setting() {
        override fun describe(): String = RikType.Mieen.desc
    }

    @Serializable
    data object Invalid : Setting()
}
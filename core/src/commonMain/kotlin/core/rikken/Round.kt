package core.rikken

data class Round(
    val setting: Setting,
    val startingPlayer: Int,
    val players: List<Int>
) {
    private val numberOfRounds = 13
    private val battles: ArrayList<Battle> = arrayListOf()

    private val rules: List<Rule> = when (setting) {
        is Setting.Rik -> listOf(MayStart(), MustConfessWithMate(setting), MustConfess(), All())
        is Setting.Solo,
        is Setting.NoRik -> listOf(MayStart(), MustConfess(), All())

        is Setting.Mieeen -> listOf(
            StartNoSchoppen(5),
            MayStart(),
            MustConfess(),
            NoSchoppen(5),
            All()
        )

        is Setting.Invalid -> emptyList()
    }

    val isNotFinished: Boolean
        get() = when (setting) {
            is Setting.NoRik -> isRoundFinished(setting)
            else -> battles.size < numberOfRounds || battles.any { it.winner() == null }
        }

    fun battles(): List<Battle> = battles

    private fun isRoundFinished(setting: Setting.NoRik): Boolean {
        val piekers = setting.piekers.isNotEmpty()
                && setting.piekers.any { p -> battles.count { it.winner() == p } <= 1 }

        val miserders =
            setting.miserders.isNotEmpty()
                    && setting.miserders.any { m -> battles.count { it.winner() == m } <= 0 }
        return battles.count { it.winner() != null } < numberOfRounds && (piekers || miserders)
    }

    /**
     * Returns the current Player
     */
    val currentPlayer: Int
        get() {
            val winner = battles.lastOrNull()?.winner()
            if (winner != null) return winner

            val lastPlayer: Int? = battles.lastOrNull()?.lastPlayer()
            return if (lastPlayer == null) {
                startingPlayer
            } else {
                (lastPlayer + 1).mod(4)
            }
        }

    fun play(player: Int, card: Card): Boolean {
        if (currentPlayer != player) return false

        val index = battles.indexOfFirst { it.winner() == null }
        val battle = battles.getOrElse(index) {
            val battle = Battle()
            battles.add(battle)
            battle
        }

        setting.played(player, card)

        battle.add(player to card)
        battle.determineWinner(setting)
        return true
    }

    fun lastBattle(): Battle? = battles.lastOrNull()

    fun allowedCardsFor(hand: List<Card>): List<Card> {
        val battle = if (lastBattle()?.isFinished() == true) Battle() else lastBattle()
        return rules.firstOrNull { rule ->
            rule.allowed(hand, battle, battles.size)
        }?.select(hand, battle) ?: emptyList()
    }
}
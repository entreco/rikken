package core.rikken

import kotlinx.serialization.Serializable

@Serializable
sealed class RikType private constructor(
    val uniqueId: Int,
    val desc: String,
    val rank: Int,
    val reward: Int,
    val multiple: Boolean = false,
    val required: List<RikType> = emptyList()
) {
    override fun toString(): String = this.desc

    @Serializable
    data object Mieen : RikType(18, "Mieëen", -2, 1)
    @Serializable
    data object None : RikType(-1, "-", -1, 0, true)
    @Serializable
    data object Pas : RikType(1, "Pas", 0, 0, true)
    @Serializable
    data object Rik : RikType(2, "Rik", 1, 2)
    @Serializable
    data object RikBeter : RikType(3, "Rik ❤", 2, 2, false, listOf(Rik))
    @Serializable
    data object Piek : RikType(4, "Piek", 3, 4, true)
    @Serializable
    data object Misere : RikType(5, "Misère", 3, 4, true)
    @Serializable
    data object Solo10 : RikType(6, "Solo 10", 4, 6)
    @Serializable
    data object Solo10Beter : RikType(7, "Solo 10 ❤", 5, 6, false, listOf(Solo10))
    @Serializable
    data object PiekOpen : RikType(8, "Piek \uD83D\uDC40", 6, 8, true)
    @Serializable
    data object MisereOpen : RikType(9, "Misère \uD83D\uDC40", 6, 8, true)
    @Serializable
    data object Solo11 : RikType(10, "Solo 11", 7, 10)
    @Serializable
    data object Solo11Beter : RikType(11, "Solo 11 ❤", 8, 10, false, listOf(Solo11))
    @Serializable
    data object PiekPraatje : RikType(12, "Piek \uD83D\uDCE3", 9, 10, true)
    @Serializable
    data object MiserePraatje : RikType(13, "Misère \uD83D\uDCE3", 9, 10, true)
    @Serializable
    data object Solo12 : RikType(14, "Solo 12", 10, 15)
    @Serializable
    data object Solo12Beter : RikType(15, "Solo 12 ❤", 11, 15, false, listOf(Solo12))
    @Serializable
    data object Solo13 : RikType(16, "Solo 13", 12, 20)
    @Serializable
    data object Solo13Beter : RikType(17, "Solo 13 ❤", 13, 20, false, listOf(Solo13))
}

val RikType.isBeter : Boolean
    get() = this == RikType.RikBeter || this == RikType.Solo10Beter || this == RikType.Solo11Beter || this == RikType.Solo12Beter || this == RikType.Solo13Beter
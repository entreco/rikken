package core.rikken

import core.rikken.RikType.Mieen
import core.rikken.RikType.Misere
import core.rikken.RikType.MisereOpen
import core.rikken.RikType.MiserePraatje
import core.rikken.RikType.None
import core.rikken.RikType.Pas
import core.rikken.RikType.Piek
import core.rikken.RikType.PiekOpen
import core.rikken.RikType.PiekPraatje
import core.rikken.RikType.Rik
import core.rikken.RikType.RikBeter
import core.rikken.RikType.Solo10
import core.rikken.RikType.Solo10Beter
import core.rikken.RikType.Solo11
import core.rikken.RikType.Solo11Beter
import core.rikken.RikType.Solo12
import core.rikken.RikType.Solo12Beter
import core.rikken.RikType.Solo13
import core.rikken.RikType.Solo13Beter
import kotlinx.serialization.Serializable

@Serializable
data class BidManager(
    private val numPlayers: Int,
    private var startingPlayer: Int
) {
    private val bids = mutableMapOf<Int, RikType>()

    init {
        repeat(numPlayers) {
            bids[it] = None
        }
    }

    private var bidCounter = startingPlayer

    /**
     * true when all players madea bit, false otherwise
     */
    val areSettled: Boolean
        get() = bids.size == numPlayers && bids.none { it.value == None }

    /**
     * currentPlayer that needs to make a bid, or null
     */
    val currentPlayer: Int
        // todo: this can probably become a bit more readable right?
        // we drop until starting
        get() = bids.toList().drop(bidCounter).firstOrNull { it.second == None }?.first
            ?: bids.toList().firstOrNull { it.second == None }?.first
            ?: (++startingPlayer).mod(numPlayers)

    operator fun plusAssign(bid: Pair<Int, RikType>) {
        val (player, type) = bid

        if (bids[player] != None) return
        if (currentPlayer != player) return

        bidCounter++

        when (type) {
            Pas -> handlePas(player)
            Rik -> handle(player, Pas, listOf(None, Pas), Rik)
            RikBeter -> handle(player, Rik, listOf(None, Pas), RikBeter)
            Misere -> handle(player, RikBeter, listOf(None, Pas, Piek, Misere), Misere)
            Piek -> handle(player, RikBeter, listOf(None, Pas, Piek, Misere), Piek)
            Solo10 -> handle(player, Misere, listOf(None, Pas), Solo10)
            Solo10Beter -> handle(player, Solo10, listOf(None, Pas), Solo10Beter)
            PiekOpen -> handle(
                player,
                Solo10Beter,
                listOf(None, Pas, PiekOpen, MisereOpen),
                PiekOpen
            )

            MisereOpen -> handle(
                player,
                Solo10Beter,
                listOf(None, Pas, PiekOpen, MisereOpen),
                MisereOpen
            )

            Solo11 -> handle(player, MisereOpen, listOf(None, Pas), Solo11)
            Solo11Beter -> handle(player, Solo11, listOf(None, Pas), Solo11Beter)
            PiekPraatje -> handle(
                player,
                Solo11Beter,
                listOf(None, Pas, PiekPraatje, MiserePraatje),
                PiekPraatje
            )

            MiserePraatje -> handle(
                player,
                Solo11Beter,
                listOf(None, Pas, PiekPraatje, MiserePraatje),
                MiserePraatje
            )

            Solo12 -> handle(player, MiserePraatje, listOf(None, Pas), Solo12)
            Solo12Beter -> handle(player, Solo12, listOf(None, Pas), Solo12Beter)
            Solo13 -> handle(player, Solo12Beter, listOf(None, Pas), Solo13)
            Solo13Beter -> handle(player, Solo13, listOf(None, Pas), Solo13Beter)
            Mieen -> handlePas(player)
            else -> {
            }
        }
    }

    fun bids(): Map<Int, RikType> = bids.toMap()

    private fun handlePas(player: Int) {
        bids[player] = Pas

        if (bids.all { it.value == Pas }) {
            bids[player] = Mieen
        }
    }

    private fun handle(player: Int, reset: RikType, allowed: List<RikType>, bid: RikType) {
        bids.reset(reset)
        if (bids.all { entry -> allowed.contains(entry.value) }) {
            bids[player] = bid
        }
    }

    private fun MutableMap<Int, RikType>.reset(rikType: RikType) {
        filter { it.value.rank > Pas.rank }
            .filter { it.value.rank <= rikType.rank }
            .forEach { bids[it.key] = None }
    }
}
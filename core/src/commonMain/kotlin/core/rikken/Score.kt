package core.rikken

import kotlinx.serialization.Serializable

@Serializable
data class Score(
    val total: Int = 0,
    val won: Int = 0,
    val history: List<RoundResult> = emptyList()
)

fun Score?.displayGain() : String = "${delta(0.05f)}"
fun Score?.displayStatus() : String = "TOTAL: ${score(0.05f)} EUR"

fun Score?.score(pricePerSlag: Float): Float = current * pricePerSlag
fun Score?.prev(pricePerSlag: Float): Float = prev * pricePerSlag
fun Score?.delta(pricePerSlag: Float): Float = delta * pricePerSlag

val Score?.current: Int
    get() = this?.total ?: 0

val Score?.delta: Int
    get() = this?.history?.lastOrNull()?.amount ?: 0

val Score?.prev: Int
    get() = (this?.total ?: 0) - this.delta

operator fun Score?.plus(amount: Int): Score {
    return this?.copy(
        total = total + amount,
        won = won.plus(if (amount > 0) 1 else 0),
        history = history + RoundResult(amount)
    ) ?: (Score() + amount)
}
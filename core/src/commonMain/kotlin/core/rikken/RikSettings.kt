package core.rikken

import RikGame
import kotlinx.serialization.Serializable

@Serializable
data class RikSettings(
    private val bids: Map<Int, RikType>,
    private val hands: List<List<Card>> = emptyList()
) {
    fun bids() = bids

    val currentPlayer: Int?
        get() = generateActions().player

    fun generateActions(): Actions {
        val action = bids.filter { it.value != RikType.Pas }.toList().first()
        return when {
            bids.any { it.value == RikType.Mieen } -> {
                Actions.ConfirmMieen
            }

            bids.any {
                it.value == RikType.Piek ||
                        it.value == RikType.Misere ||
                        it.value == RikType.PiekOpen ||
                        it.value == RikType.MisereOpen ||
                        it.value == RikType.PiekPraatje ||
                        it.value == RikType.MiserePraatje
            } -> {
                Actions.ConfirmNoRik(action.second)
            }

            bids.any {
                it.value == RikType.Solo10 ||
                        it.value == RikType.Solo10Beter ||
                        it.value == RikType.Solo11 ||
                        it.value == RikType.Solo11Beter ||
                        it.value == RikType.Solo12 ||
                        it.value == RikType.Solo12Beter ||
                        it.value == RikType.Solo13
            } -> {
                Actions.ConfirmRik.Solo(action.first, action.second)
            }

            else -> {
                Actions.ConfirmRik.Normal(
                    action.first,
                    action.second,
                    excludedAsMate(hands[action.first])
                )
            }
        }
    }

    private fun excludedAsMate(hand: List<Card>?): Map<Suit, List<Suit>> {
        if (hand == null) return emptyMap()

        val mates = mutableMapOf<Suit, List<Suit>>()
        mates[Suit.HEARTS] = excludesFor(Suit.HEARTS, hand)
        mates[Suit.CLUBS] = excludesFor(Suit.CLUBS, hand)
        mates[Suit.DIAMONDS] = excludesFor(Suit.DIAMONDS, hand)
        mates[Suit.SPADES] = excludesFor(Suit.SPADES, hand)
        return mates
    }

    private fun excludesFor(troef: Suit, hand: List<Card>): List<Suit> {
        val grouped = hand.sorted().filter { it.suit != troef }.groupBy { it.suit }
        val blocked = grouped.filterValues { it.any { it.rank == Rank.ACE } }
        val kings = if (blocked.size == 3) grouped.filterValues { it.any { it.rank == Rank.KING } } else blocked
        val queens = if (kings.size == 3) grouped.filterValues { it.any { it.rank == Rank.QUEEN } } else kings
        val jacks = if (queens.size == 3) grouped.filterValues { it.any { it.rank == Rank.JACK } } else queens
        return jacks.map { it.key }
    }

    fun create(event: RikGame.Event.Confirm): Setting = when(event){
        is RikGame.Event.Confirm.Rik -> validateRik(event)
        is RikGame.Event.Confirm.Solo -> validateSolo(event)
        is RikGame.Event.Confirm.NoRik -> validateNoRik(event)
        is RikGame.Event.Confirm.Mieeen -> Setting.Mieeen
    }

    private fun validateRik(event: RikGame.Event.Confirm.Rik): Setting {
        if (event.troef == event.maat) return Setting.Invalid

        val (hand, maat) = validateMaat(event)
        if (hand.any { it == maat }) return Setting.Invalid

        val bid = bids.filter { it.key == event.playerId.index }.toList().first()
        return when (bid.second) {
            RikType.Rik -> Setting.Rik(event.playerId.index, event.troef, maat, event.rikType, 8)
            RikType.RikBeter -> Setting.Rik(event.playerId.index, Suit.HEARTS, maat, event.rikType, 8)
            else -> Setting.Invalid
        }
    }

    private fun validateMaat(event: RikGame.Event.Confirm.Rik): Pair<List<Card>, Card> {
        val hand = hands[event.playerId.index]
        val aces = hand.sorted().filter { it.suit != event.troef }.filter { it.rank == Rank.ACE }
        val kings = hand.sorted().filter { it.suit != event.troef }.filter { it.rank == Rank.KING }
        val maat = when {
            aces.none { it.suit == event.maat } -> Card(Rank.ACE, event.maat)
            kings.none { it.suit == event.maat } -> Card(Rank.KING, event.maat)
            else -> Card(Rank.QUEEN, event.maat)
        }
        return Pair(hand, maat)
    }

    private fun validateSolo(event: RikGame.Event.Confirm.Solo): Setting {
        val bid = bids[event.playerId.index]
        return when (bid) {
            RikType.Solo10 -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 10)
            RikType.Solo10Beter -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 10)
            RikType.Solo11 -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 11)
            RikType.Solo11Beter -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 11)
            RikType.Solo12 -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 12)
            RikType.Solo12Beter -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 12)
            RikType.Solo13 -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 13)
            RikType.Solo13Beter -> Setting.Solo(event.playerId.index, event.troef, event.rikType, goal = 13)
            else -> Setting.Invalid
        }
    }

    private fun validateNoRik(event: RikGame.Event.Confirm.NoRik): Setting {
        val piekers = bids.filter { it.value == RikType.Piek || it.value == RikType.PiekOpen || it.value == RikType.PiekPraatje }.map { it.key }
        val miserders = bids.filter { it.value == RikType.Misere || it.value == RikType.MisereOpen || it.value == RikType.MiserePraatje }.map { it.key }

        return if (piekers.isEmpty() and miserders.isEmpty()) Setting.Invalid
        else Setting.NoRik(piekers, miserders, event.rikType)
    }
}

sealed class Actions(open val player: Int?) {
    sealed class ConfirmRik(override val player: Int, open val rikType: RikType) : Actions(player) {
        data class Normal(
            override val player: Int,
            override val rikType: RikType,
            val excludes: Map<Suit, List<Suit>>
        ) : ConfirmRik(player, rikType)

        data class Solo(override val player: Int, override val rikType: RikType) :
            ConfirmRik(player, rikType)
    }

    data class ConfirmNoRik(val rikType: RikType) : Actions(null)
    data object ConfirmMieen : Actions(null)
}

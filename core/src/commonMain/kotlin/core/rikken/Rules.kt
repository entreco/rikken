package core.rikken

internal interface Rule {
    fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean
    fun select(hand: List<Card>, battle: Battle?): List<Card>
}

/**
 * Nieuwe Slag -> Speler mag uitkomen
 */
internal class MayStart : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int) = battle == null || battle.isEmpty()
    override fun select(hand: List<Card>, battle: Battle?) = hand
}

/**
 * Moet Maat kaart gooien
 */
internal class MustConfessWithMate(
    private val setting: Setting.Rik
) : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean {
        requireNotNull(battle) { "Battle is null" }
        return battle.suit == setting.mate.suit && hand.contains(setting.mate)
    }

    override fun select(hand: List<Card>, battle: Battle?): List<Card> {
        return hand.filter { it == setting.mate }
    }
}

/**
 * Moet bekennen
 */
internal class MustConfess : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean {
        requireNotNull(battle) { "Battle is null" }
        return hand.any { it.suit == battle.suit }
    }

    override fun select(hand: List<Card>, battle: Battle?): List<Card> {
        requireNotNull(battle) { "Battle is null" }
        return hand.filter { it.suit == battle.suit }
    }
}

/**
 * 1e 5 rondes geen schoppen
 */
internal class StartNoSchoppen(val limit: Int) : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean {
        return (battle == null || battle.isEmpty()) && round < limit
    }

    override fun select(hand: List<Card>, battle: Battle?): List<Card> {
        return if (hand.any { it.suit == Suit.SPADES })
            hand.filter { it.suit != Suit.SPADES }
        else {
            hand
        }
    }
}

/**
 * No Spades when Mieeen && first 5 rounds,
 * except if you ONLY have Spades
 */
internal class NoSchoppen(val limit: Int) : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean {
        requireNotNull(battle) { "Battle is null" }
        return round <= limit
    }

    override fun select(hand: List<Card>, battle: Battle?): List<Card> {
        requireNotNull(battle) { "Battle is null" }
        return if (hand.any { it.suit != Suit.SPADES })
            hand.filter { it.suit != Suit.SPADES }
        else
            hand
    }
}

/**
 * Vrije Keus, if all other rules are not applicable
 */
internal class All : Rule {
    override fun allowed(hand: List<Card>, battle: Battle?, round: Int): Boolean {
        requireNotNull(battle) { "Battle is null" }
        return true
    }

    override fun select(hand: List<Card>, battle: Battle?): List<Card> {
        requireNotNull(battle) { "Battle is null" }
        return hand
    }
}

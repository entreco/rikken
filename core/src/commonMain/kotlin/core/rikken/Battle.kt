package core.rikken

/**
 * A single battle or 'Slag' in Dutch.
 * Each player has to show a single Card.
 * Based on the GameSettings, this determines who
 * may start next
 */
class Battle {

    private val cards: MutableList<Pair<Int, Card>> = mutableListOf()
    private var winner: Int? = null

    /**
     * The starting Suit of this Battle
     */
    val suit: Suit?
        get() = cards.firstOrNull()?.second?.suit

    fun isEmpty() = cards.isEmpty()
    fun isLast() = cards.count() == 3
    fun winner() = winner
    fun isFinished() = winner != null
    fun cardOf(player: Int) = cards.firstOrNull { it.first == player }?.second
    fun playerAt(index: Int) = cards.getOrNull(index)?.first
    fun cardAt(index: Int) = cards.getOrNull(index)?.second
    fun lastPlayer() = cards.lastOrNull()?.first

    /**
     * Useful to determine if ♠Q is present
     * @return true if this battle contains the specified card
     */
    fun containsCard(card: Card) = cards.any { it.second == card }

    fun add(playerCardPair: Pair<Int, Card>) {
        cards.add(playerCardPair)
    }

    fun highest(): Pair<Int, Card> =
        cards.filter { it.second.suit == suit }.maxBy { it.second.rank }

    /**
     * Based on the Settings, will determine who
     * won this Battle. E.g. for Mieeen, there's no troef.
     * @param setting Settings used in this Battle
     */
    fun determineWinner(setting: Setting) {
        winner = when (setting) {
            is Setting.Solo -> findWinner(setting.troef)
            is Setting.Rik -> findWinner(setting.troef)
            is Setting.NoRik -> findWinner()
            is Setting.Mieeen -> findWinner()
            else -> null
        }
    }

    private fun findWinner(troef: Suit? = null): Int? {
        if (cards.size < 4) return null
        return when {
            suit == null -> null
            troef != null && cards.any { it.second.suit == troef } -> cards.filter { it.second.suit == troef }
                .maxByOrNull { it.second.rank }?.first

            else -> cards.filter { it.second.suit == suit }.maxByOrNull { it.second.rank }?.first
        }
    }

    override fun toString(): String = "$suit w:${winner}"
}

package core.rikken

object RikTypeRules {
    internal val all : List<RikType> = listOf(
        RikType.None,
        RikType.Pas,
        RikType.Rik,
        RikType.RikBeter,
        RikType.Piek,
        RikType.Misere,
        RikType.Solo10,
        RikType.Solo10Beter,
        RikType.PiekOpen,
        RikType.MisereOpen,
        RikType.Solo11,
        RikType.Solo11Beter,
        RikType.PiekPraatje,
        RikType.MiserePraatje,
        RikType.Solo12,
        RikType.Solo12Beter,
        RikType.Solo13,
        RikType.Solo13Beter,
        RikType.Mieen
    )

    fun from(uniqueId: Int): RikType = all.first { it.uniqueId == uniqueId }

    fun allowed(bids: Map<Int, RikType>): List<RikType> {
        val max: RikType =
            bids.filterValues { it.rank >= RikType.Pas.rank }.maxByOrNull { it.value.rank }
                ?.toPair()?.second ?: RikType.Pas
        val required = if (allPass(bids)) listOf(RikType.Mieen) else listOf(RikType.Pas)

        return (required + all
            .filter { it !is RikType.Pas }
            .greaterThan(max)
            .hasRequired(bids))
            .distinct()
    }

    private fun allPass(bids: Map<Int, RikType>) = bids.count { it.value == RikType.Pas } == 3
}

private fun List<RikType>.greaterThan(max: RikType) = filter { type ->
    if (max.multiple) {
        type.rank >= max.rank
    } else type.rank > max.rank
}

private fun List<RikType>.hasRequired(bids: Map<Int, RikType>) = filter { allowed ->
    allowed.required.isEmpty() or allowed.required.all { bids.containsValue(it) }
}

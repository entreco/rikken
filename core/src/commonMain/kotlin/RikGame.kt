import core.rikken.Card
import core.rikken.RikSettings
import core.rikken.RikType
import core.rikken.Score
import core.rikken.Setting
import core.rikken.Suit
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.random.Random

@Serializable
sealed class RikGame {

    companion object {
        var lastId: Long = 0

        fun String?.deserialize(): RikGame = when {
            this == null -> None
            startsWith("/settling") -> Json.decodeFromString<State.Settling>(removePrefix("/settling").trim())
            startsWith("/playing") -> Json.decodeFromString<State.Playing>(removePrefix("/playing").trim())
            startsWith("/selecting") -> Json.decodeFromString<State.Selecting>(removePrefix("/selecting").trim())
            startsWith("/bidding") -> Json.decodeFromString<State.Bidding>(removePrefix("/bidding").trim())
            startsWith("/waiting") -> Json.decodeFromString<State.Waiting>(removePrefix("/waiting").trim())
            startsWith("/welcome") -> Json.decodeFromString<State.Welcome>(removePrefix("/welcome").trim())

            startsWith("/confirm ") -> Json.decodeFromString<Event.Confirm>(removePrefix("/confirm").trim())
            startsWith("/confirmRik ") -> Json.decodeFromString<Event.Confirm.Rik>(removePrefix("/confirmRik").trim())
            startsWith("/confirmNoRik ") -> Json.decodeFromString<Event.Confirm.NoRik>(removePrefix("/confirmNoRik").trim())
            startsWith("/confirmSolo ") -> Json.decodeFromString<Event.Confirm.Solo>(removePrefix("/confirmSolo").trim())
            startsWith("/confirmMieeen ") -> Json.decodeFromString<Event.Confirm.Mieeen>(removePrefix("/confirmMieeen").trim())

            startsWith("/begin") -> Json.decodeFromString<Event.Begin>(removePrefix("/begin").trim())
            startsWith("/chat") -> Json.decodeFromString<Event.Chat>(removePrefix("/chat").trim())
            startsWith("/confirm") -> Json.decodeFromString<Event.Confirm>(removePrefix("/confirm").trim())
            startsWith("/play") -> Json.decodeFromString<Event.Play>(removePrefix("/play").trim())
            startsWith("/bid") -> Json.decodeFromString<Event.Bid>(removePrefix("/bid").trim())
            else -> None
        }
    }

    fun serialize(): String = when (this) {
        is State.Settling -> "/settling ${Json.encodeToString(this)}"
        is State.Playing -> "/playing ${Json.encodeToString(this)}"
        is State.Selecting -> "/selecting ${Json.encodeToString(this)}"
        is State.Bidding -> "/bidding ${Json.encodeToString(this)}"
        is State.Waiting -> "/waiting ${Json.encodeToString(this)}"
        is State.Welcome -> "/welcome ${Json.encodeToString(this)}"
        is Event.Begin -> "/begin ${Json.encodeToString(this)}"
        is Event.Chat -> "/chat ${Json.encodeToString(this)}"
        is Event.Play -> "/play ${Json.encodeToString(this)}"
        is Event.Bid -> "/bid ${Json.encodeToString(this)}"
        is Event.Confirm -> "/confirm ${Json.encodeToString(this)}"
        is Event.Confirm.Rik ->  "/confirmRik ${Json.encodeToString(this)}"
        is Event.Confirm.NoRik ->  "/confirmNoRik ${Json.encodeToString(this)}"
        is Event.Confirm.Solo ->  "/confirmSolo ${Json.encodeToString(this)}"
        is Event.Confirm.Mieeen ->  "/confirmMieeen ${Json.encodeToString(this)}"
        is None -> "/none"
    }

    @Serializable
    data object None : RikGame()

    @Serializable
    sealed class State : RikGame() {

        @Serializable
        data class Welcome(val version: String) : State()

        @Serializable
        data class Waiting(
            val index: Int,
            val code: RoomID,
            val playersRemaining: Int,
            val error: String? = null
        ) : State()

        @Serializable
        data class Bidding(
            val players: List<Rikker>,
            val hands: List<List<Card>>,
            val startingPlayer: Int = 0,
            val biddingPlayer: Int,
            val bids: Map<Int, RikType>,
            val error: Oops? = null,
        ) : State()

        @Serializable
        data class Selecting(
            val players: List<Rikker>,
            val hands: List<List<Card>>,
            val startingPlayer: Int,
            val rikSettings: RikSettings,
            val error: Oops? = null,
        ) : State()

        @Serializable
        data class Playing(
            val players: List<Rikker>,
            val hands: List<List<Card>>,
            val startingPlayer: Int,
            val currentPlayer: Int,
            val settings: Setting,
            val allowed: List<Card>,
            val played: List<Pair<Int,Card>> = emptyList(),
            val winners: List<Int> = emptyList(),
            val error: Oops? = null,
        ) : State()

        @Serializable
        data class Settling(
            val players: List<Rikker>,
            val hands: List<List<Card>>,
            val startingPlayer: Int,
            val settings: Setting,
            val scores: Map<Int, Score>,
            val error: Oops? = null,
        ) : State()
    }

    @Serializable
    sealed class Event : RikGame() {

        @Serializable
        data class Chat(val msg: String, val time: Long = lastId++) : Event()

        @Serializable
        data class Begin(val roomId: RoomID) : Event()

        @Serializable
        data class Bid(val playerId: PlayerId, val rikType: RikType) : Event()

        @Serializable
        sealed class Confirm : Event(){
            abstract val playerId: PlayerId
            abstract val rikType: RikType

            @Serializable
            data class Rik(
                override val playerId: PlayerId,
                val troef: Suit,
                val maat: Suit,
                override val rikType: RikType
            ) : Confirm()

            @Serializable
            data class Solo(
                override val playerId: PlayerId,
                val troef: Suit,
                override val rikType: RikType
            ) : Confirm()

            @Serializable
            data class NoRik(override val playerId: PlayerId, override val rikType: RikType) : Confirm()

            @Serializable
            data class Mieeen(override val playerId: PlayerId) : Confirm() {
                override val rikType = RikType.Mieen
            }
        }

        @Serializable
        data class Play(
            val playerId: PlayerId,
            val index: Int,
            val rand: Long = Random.nextLong()
        ) : Event()

    }
}




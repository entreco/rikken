import kotlinx.serialization.Serializable
import kotlin.jvm.JvmInline
import kotlin.random.Random

@JvmInline
@Serializable
value class RoomID(val code: String) {

    init {
        require(code.length == NUM_CHARS) { "Invalid RoomID" }
    }

    companion object {

        const val NUM_CHARS = 5
        private const val ALLOWED = "ABCDEFGHJKMNPQRSTUVWXYZ23456789"

        fun random(): RoomID = RoomID(buildString {
            repeat(NUM_CHARS) {
                val index = Random.nextInt(ALLOWED.length)
                append(ALLOWED[index])
            }
        })

        fun String?.asRoomID(): RoomID =
            if (isNullOrBlank() || length != NUM_CHARS) random() else RoomID(this)
    }
}
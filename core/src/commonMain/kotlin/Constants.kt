const val RIK_VERSION = "0.0.4"

sealed class Settings(val host: String, val port: Int?) {
    data object Local : Settings("192.168.50.34", 8080)
    data object Test : Settings("localhost", 8080)
    data object Heroku : Settings("rikken-c78ab7ac4e1f.herokuapp.com", null)
    data object AppEngine : Settings("rikken-426410.ew.r.appspot.com", 443)
}
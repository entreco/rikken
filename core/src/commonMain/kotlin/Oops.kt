import kotlinx.serialization.Serializable

@Serializable
data class Oops(val to: Int, val message: String)